$(window).load(function(){
/** Carousel of items *
 START -------------> 
	if($('.jcarousel').lenght > 0){
		$(function() {
			$('.jcarousel').jcarousel({});
			
			$('.jcarousel-prev').jcarouselControl({
				target: '-=1'
			});

			$('.jcarousel-next').jcarouselControl({
				target: '+=1'
			});
		});
	}
	/** Carousel of items  *
 END ---------------|  */
 
/* smooth scrolling for scroll to top */
$('.scroll-top').click(function(){
	$('body,html').animate({scrollTop:0},1000);
});
 
 
/** General gdivMessage START *
* * * * * * * * * * * * * * * */
$('body').on('click', ".gdivMessage", function(){ $(this).fadeOut('slow', function(){  } ); });

function gdivMessageAdd(message, type){
	if(!message)
		message = 'Scusate per il disaggio.';
	if(!type)
		type = 'warning';
		
	var zindex = parseInt($(".withAlert").length) + 1;
		// margin-top:'+ 45 * wCount +'px;
	var element = '<div style="z-index: '+ zindex +'" class="alert alert-'+type+' alert-dismissable withAlert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><span class="message">'+message+'</span></div>';
	
	var eobj = $(element).clone();
	
	$(eobj).appendTo("body");
	
	setTimeout(function(){$(eobj).hide('slow', function(){$(this).remove();});}, 4000);
}


/* * * * * * * * * * * * * * * * 
gdivMessageAdd('messagggio', 'danger');
* * General gdivMessage END **/

/* * * * * * * * * * * * * * * *
// function for collapse a panel START */
$(".withBox").on('click', ".showBox", function(){
    var thisBox = $(this).parent().parent();
    $(".shBox", thisBox).show();
});
$(".withBox").on('click', ".hideBox", function(){
    var thisBox = $(this).parent().parent();
    $(".shBox", thisBox).hide();
});
/* showhideBox (toggle)
 * - <withBox [collapsable]>
 * - - <showhideBox> [<shSwitch> & <shSwitch>]
 * - - - <shBox>
 * - </>
 */
$(".withBox").on('click', ".showhideBox", function(){
	var thisBox = $(this).parent();
    $(".shBox", thisBox).toggle('fast');
    $(".shSwitch", thisBox).toggle(1, function(){ 
		if($(this).hasClass('set'))
			$(this).removeClass('set');
		else
			$(this).addClass('set');
	});
});
// no close on himself click
$(".withBox.collapsable").mouseup(function(){ return false; });
// close on document click
$(document).mouseup(function(){
    $(".shBox", $(".withBox.collapsable")).slideUp();
    var shSwitch = $(".shSwitch",  $(".withBox.collapsable"));
	if(shSwitch.hasClass('set'))
		shSwitch.toggle(1, function(){ 
		if($(this).hasClass('set'))
			$(this).removeClass('set');
		else
			$(this).addClass('set');
	});
});
/*  // function for collapse a panel END
* * * * * * * * * * * * * * * */

/* * * * * * * *           * * * * * * * * * * *
 * @documentclick @general with all this method *
 * * * * /
$(document).mouseup(function(){
    
});*/




/* MANAGE scriptS */
/*-*-*-*-*-*-*-*-*-*START*-*-*-*-*-*-*-*-*-*-*-
*-*-*-*-*-*    File Upload Func    *-*-*-*-*-*/
 /*
 * jQuery File Upload Plugin JS Example 8.9.0
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
 
/*jslint unparam: true, regexp: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    //var url = window.location.hostname === 'blueimp.github.io' ? '//jquery-file-upload.appspot.com/' : '/public/js/blueimpup/server/php/',
	//var url = '/public/js/blueimpup/server/php/property.php',
    var url = '/property/biupload',
        uploadButton = $('<button/>')
            .addClass('btn btn-primary')
            .prop('disabled', true)
            .text('Caricamento...')
            .on('click', function () {
                var $this = $(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Annulla')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });
			
	$('.files').on('click', '.cancel', function(){
		$(this).parent().parent().remove();
	});
	$('.fileupload').each(function () {
		$(this).fileupload({
			url: $(this).attr('action'),
			dataType: 'json',
			autoUpload: true,
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			maxFileSize: 5000000, // 5 MB
			// Enable image resizing, except for Android and Opera,
			// which actually support image resizing, but fail to
			// send Blob objects via XHR requests:
			disableImageResize: /Android(?!.*Chrome)|Opera/
				.test(window.navigator.userAgent),
			previewMaxWidth: 150,
			previewMaxHeight: 150,
			//sequentialUploads: true,
			limitConcurrentUploads: 3,
			disableImageMetaDataLoad: true,
			disableImageMetaDataSave: true,
			dropZone: $(this).parent()
		}).on('fileuploadadd', function (e, data) {
			data.context = $('<div/>').appendTo('#files');
			$.each(data.files, function (index, file) {
				var node = $('<p/>')
						.append($('<span/>').text(file.name));
				if (!index) {
					node
						.append('<br>')
						.append(uploadButton.clone(true).data(data));
				}
				node.appendTo(data.context);
			});
		}).on('fileuploadprocessalways', function (e, data) {
			var index = data.index,
				file = data.files[index],
				node = $(data.context.children()[index]);
			if (file.preview) {
				node
					.prepend('<br>')
					.prepend(file.preview);
			}
			if (file.error) {
				node
					.append('<br>')
					.append($('<span class="text-danger"/>').text(file.error));
			}
			if (index + 1 === data.files.length) {
				data.context.find('button')
					.text('Annulla');// .prop('disabled', !!data.files.error);
			}
		}).on('fileuploadprogressall', function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#progress .progress-bar').css(
				'width',
				progress + '%'
			);
		}).on('fileuploaddone', function (e, data) {
			$.each(data.result.files, function (index, file) {
                if(file.url && file.poster){
                    $(".poster-preview", $(".poster-files")).remove();
                    $(".poster-error", $(".poster-files")).remove();
                    $(".poster-btn", $(".poster-files")).remove();
                }
                setTimeout(function(){
                    if (file.url) {
                        var link = $('<a>')
                            .attr('target', '_blank')
                            .prop('href', file.url);
                        $(data.context.children()[index])
                            .wrap(link);

                        if(file.poster){
                            $(".preview", $(".poster-files")).addClass("poster-preview");
                            $(".btn", $(".poster-files")).addClass("poster-btn");
                        }
                    } else if (file.error) {
                        var error = $('<span class="text-danger"/>').text(file.error);
                        $(data.context.children()[index])
                            .append('<br>')
                            .append(error);

                        if(file.poster){
                            $(".error", $(".poster-files")).addClass("poster-error");
                        }
                    }
                }, 1500);
			});
		}).on('fileuploadfail', function (e, data) {
			$.each(data.files, function (index, file) {
				var error = $('<span class="text-danger"/>').text('Upload file fallito.');
				$(data.context.children()[index])
					.append('<br>')
					.append(error);
			});
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');
	});	
});

$(document).bind('dragover', function (e) {
    var dropZone = $('.dropzone'),
        timeout = window.dropZoneTimeout;
    if (!timeout) {
        dropZone.addClass('in');
    } else {
        clearTimeout(timeout);
    }
    var found = false,
      	node = e.target;
    do {
        if (node === dropZone[0]) {
       		found = true;
       		break;
       	}
       	node = node.parentNode;
    } while (node != null);
    if (found) {
        dropZone.addClass('hover');
    } else {
        dropZone.removeClass('hover');
    }
    window.dropZoneTimeout = setTimeout(function () {
        window.dropZoneTimeout = null;
        dropZone.removeClass('in hover');
    }, 100);
});

$(document).bind('drop dragover', function (e) {
    e.preventDefault();
});
/*-*-*-*-*-*-*-*-*-*END*-*-*-*-*-*-*-*-*-*-*-
*-*-*-*-*-*    File Upload Func    *-*-*-*-*-*/
 
}); // END $(window).load( function{...




