<?php
error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');

class CustomUploadHandler extends UploadHandler {
	var $mysql = array('delete_type' => 'POST',  'db_port' => 3306,'db_table' => 'ast_files');
	

	public function __construct($opts){
		parent::__construct($opts);
		
	}

    protected function initialize() {
		if(stripos('ast.loc', $_SERVER['HTTP_HOST']) !== FALSE){
			$this->db = new mysqli('localhost', 'root', '', 'its_erik');
			$this->mysql['db_name'] = 'its_erik';
		} else {
			$this->db = new mysqli('sql.aidashoptour.com', 'aidashop78475', 'bahtooce', 'aidashop78475', 3306);
			$this->mysql['db_name'] = 'aidashop78475';
		}
		
        parent::initialize();
        $this->db->close();
    }

    protected function handle_form_data($file, $index) {
    	//$file->title = @$_REQUEST['title'][$index];
    	$file->description = @$_REQUEST['description'][$index];
    	$file->rel_id = @$_REQUEST['rel_id'];
    	$file->rel_type = @$_REQUEST['rel_type'];
    }
	
	/*protected function trim_file_name($file_path, $name, $size, $type, $error, $index, $content_range) {
        $name = 'property_'.date('Y-m-d').'T'.date('H:i:s').'-'.uniqid();
        return parent::trim_file_name($file_path, $name, $size, $type, $error, $index, $content_range);
    }*/

    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error, $index = null, $content_range = null){
		$file = parent::handle_file_upload($uploaded_file, $name, $size, $type, $error, $index, $content_range);
		$file->title = $name;
        if (empty($file->error)) {
			
			$sql = "UPDATE `ast_tour` SET `poster`='{$name}' WHERE `id`={$file->rel_id}";
			$this->db->query($sql);
        }
		
		// set_additional_file_properties
		$file->deleteUrl = $this->options['script_url'].'poster_tour.php'.$this->get_query_separator($this->options['script_url']).'rel_id'.'='.$file->rel_id.'&_method=DELETE&'.$this->get_singular_param_name().'='.rawurlencode($file->name);
		
        return $file;
    }
/*
    protected function set_additional_file_properties($file) {
        parent::set_additional_file_properties($file);
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        	$sql = 'SELECT `id`, `type`, `title`, `description` FROM `'
        		.$this->mysql['db_table'].'` WHERE `name`=?';
        	$query = $this->db->prepare($sql);
 	        $query->bind_param('s', $file->name);
	        $query->execute();
	        $query->bind_result(
	        	$id,
	        	$type,
	        	$title,
	        	$description
	        );
	        while ($query->fetch()) {
	        	$file->id = $id;
        		$file->type = $type;
        		$file->title = $title;
        		$file->description = $description;
    		}
        }
    }
*/
    public function delete($print_response = true) {
        $response = parent::delete(false);
		
		$sql = "UPDATE `ast_tour` SET `poster`='' WHERE `id`={$_GET['rel_id']}";
		$this->db->query($sql);

        return true;//$this->generate_response($response, $print_response);
    }

}

$upload_handler = new CustomUploadHandler(array(
	'upload_dir' => dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/files/tour/images/',
	'upload_url' => 'http://'.$_SERVER['SERVER_NAME'].'/public/files/tour/images/',
	'access_control_allow_origin' => '',
	// Defines which files can be displayed inline when downloaded:
	'inline_file_types' => '/\.(gif|jpe?g|png)$/i',
	// Defines which files (based on their names) are accepted for upload:
	'accept_file_types' => '/.+$/i',
	// The php.ini settings upload_max_filesize and post_max_size
	// take precedence over the following max_file_size setting:
	'max_file_size' => 5 * 1024 * 1024, // 3 MiB
	'min_file_size' => 1,
	// The maximum number of files for the upload directory:
	'max_number_of_files' => null,
	// Defines which files are handled as image files:
	'image_file_types' => '/\.(gif|jpe?g|png)$/i',
	
	'image_versions' => array(
		// The empty image version key defines options for the original image:
		'' => array(
			// Automatically rotate images based on EXIF meta data:
			'auto_orient' => true
		),
		// Uncomment the following to create medium sized images:
		
		'medium' => array(
			'max_width' => 800,
			'max_height' => 600
		),
		
		'thumbnail' => array(
			// Uncomment the following to use a defined directory for the thumbnails
			// instead of a subdirectory based on the version identifier.
			// Make sure that this directory doesn't allow execution of files if you
			// don't pose any restrictions on the type of uploaded files, e.g. by
			// copying the .htaccess file from the files directory for Apache:
			//'upload_dir' => dirname($this->get_server_var('SCRIPT_FILENAME')).'/thumb/',
			//'upload_url' => $this->get_full_url().'/thumb/',
			
			// Uncomment the following to force the max
			// dimensions and e.g. create square thumbnails:
			'crop' => true,
			'max_width' => 250,
			'max_height' => 250
		)
	)
));
