 /*
 * jQuery File Upload Plugin JS Example 8.9.0
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
 
/*jslint unparam: true, regexp: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    //var url = window.location.hostname === 'blueimp.github.io' ? '//jquery-file-upload.appspot.com/' : '/public/js/blueimpup/server/php/',
    var url = '/app/public/js/blueimpup/server/php/property.php',
        uploadButton = $('<button/>')
            .addClass('btn btn-primary')
            .prop('disabled', true)
            .text('Caricamento...')
            .on('click', function () {
                var $this = $(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Annulla')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });
			
	$('#fileupload').on('click', '.cancel', function(){
		$(this).parent().parent().remove();
	});
			
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 3000000, // 3 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
		//sequentialUploads: true,
		limitConcurrentUploads: 2,
		disableImageMetaDataLoad: true,
		disableImageMetaDataSave: true,
		dropZone: $('#dropzone')
    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>')
                    .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Annulla');// .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index, file) {
            var error = $('<span class="text-danger"/>').text('Upload file fallito.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

$(document).bind('dragover', function (e) {
    var dropZone = $('#dropzone'),
        timeout = window.dropZoneTimeout;
    if (!timeout) {
        dropZone.addClass('in');
    } else {
        clearTimeout(timeout);
    }
    var found = false,
      	node = e.target;
    do {
        if (node === dropZone[0]) {
       		found = true;
       		break;
       	}
       	node = node.parentNode;
    } while (node != null);
    if (found) {
        dropZone.addClass('hover');
    } else {
        dropZone.removeClass('hover');
    }
    window.dropZoneTimeout = setTimeout(function () {
        window.dropZoneTimeout = null;
        dropZone.removeClass('in hover');
    }, 100);
});

$(document).bind('drop dragover', function (e) {
    e.preventDefault();
});



/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-START
*-*-*-*-*-*    File Upload Func    *-*-*-*-*-*/

// if isset only one dropZone initialize fileDropZoneFnc, and append a fnc to input button (for show error if file not selected)
if($(".fileDropZone")[0]){
    fileDropZoneFnc();
    
    $(".fileDropZone [type=submit]").click( function() {
        var thisBox = $(this).parent().parent();
        if(!$("input[type=file]", thisBox).val()){
            $("#fileError").clone().prependTo(thisBox).show();
            setTimeout(function(){$("#fileError", thisBox).fadeOut('slow', function(){$("#fileError", thisBox).remove()});},3500);
            return false;
        } else
            fileCheckProgress($(".prgrsId", thisBox).val(), thisBox);
    });
}

// switch from view file to upload file
$(".jFileHere").on('click', '.fileEdit', function(){
    if(!$(this).hasClass('disabled')){
        var thisBox = $(this).parent();
        
        $(".divSwitch", thisBox).toggle();
        $(".btnSwitch", thisBox).toggle();
    }
});
// switch from upload file to view file
$(".jFileHere").on('click', ".fileCancel", function(){
    if(!$(this).hasClass('disabled')){
        var thisBox = $(this).parent();
        $(".divSwitch", thisBox).toggle();
        $(".btnSwitch", thisBox).toggle();
    }
});

/**
 * Get all fileDropZone domObject, understand what of it active and call the fileUploadFnc
 * 
 */
function fileDropZoneFnc(){
    var dZone = $(".fileDropZone"), thisBox;

    dZone[0].ondragover = function(){
        thisBox = $(this).parent().parent();
        fileUploadFnc(dZone[0], thisBox);
    };
    // if browser not support drag&drop write message to upload via form
    if(typeof(window.FileReader) == 'undefined'){
        $("p", dZone).text('Caricare dalla form classica');
    }
}

/**
 * Capture drag and drop action for interact with user, ondrop send a XHR to server for upload image, on complete call finishUploadFile for give to user a message
 * 
 * @param {dom/object} dropZone
 * @param {dom/object} thisBox
 * 
 * @return {undefined}
 */
function fileUploadFnc(dropZone, thisBox){

    dropZone.ondragover = function(){
        $(".fileDropZone", thisBox).removeClass('drop');
        $(".fileDropZone", thisBox).addClass('hover');
        return false;
    };
    dropZone.ondragleave = function(){
        $(".fileDropZone", thisBox).removeClass('hover');
        return false;
    };

    dropZone.ondrop = function(event){
        event.preventDefault();
        $(".fileDropZone", thisBox).removeClass('hover');
        $(".fileDropZone", thisBox).addClass('drop');

        var file = event.dataTransfer.files[0];
        $("p", dropZone).removeClass('text-danger').html('<p>File: <strong>"'+file.name+'"</strong></p>');
        $("input[type=file]", thisBox).hide();
        
        var formData = new FormData();

        // Append our file to the formData object
        // Notice the first argument "file" and keep it in mind
        formData.append('file', file);
       

        var item = $(".fileDropZone", thisBox).attr('id');
        formData.append('item', item);
        var id = $("#itemHiddenId").val();
        formData.append('id', id);
		
        var toUrl = '/app/property/fileajaxupload';
        
        var xhr = new XMLHttpRequest();
        var prgrsBar = $(".progressBar", dropZone);
        $(".bar", prgrsBar).attr('style', 'width: 0%;');
        prgrsBar.removeClass('progress-danger').addClass('progress-info').show();

        xhr.upload.onprogress = function(e) {
            if(e.lengthComputable){
                $(".bar", prgrsBar).attr('style', 'width:'+ (e.loaded / e.total) * 100 +'%;');
            }
        };
        xhr.onreadystatechange = function(){
                if(4 == this.readyState ){
                    $("p", dropZone).append(".. Wait ...");
                    setTimeout(function(){finishUploadFile(item)},7000);
                }
            };
        xhr.open('POST', toUrl);
        //xhr.setRequestHeader('X-FILE-NAME', file.name);
        xhr.send(formData);

        $("p", dropZone).append(".. Upload ..");
    };


    /**
     * Send AJAX request to server for know if file loaded or not, receive jSON data and tell to user what happenned
     * 
     * @param {string} item
     * @return {undefined}
     */
    function finishUploadFile(item){
        var id = $("#itemHiddenId").val(), prgrsBar = $(".progressBar", dropZone);

        var toUrl = '/app/property/fileajaxupload';
        
        $.ajax({
             url: toUrl,
             data: {id: id, item: item},
             dataType: 'json',
             type: 'POST',
             success: function(json){
                if(json.success){
                    if(item == 'image'){
                        setTimeout(function(){
                            $(".divSwitch", thisBox).toggle();
                            $(".resUrl", thisBox).attr("src", "/app/public/files/"+json.fname);
                        }, 3000);
                    } else {
                        $(".divSwitch", thisBox).toggle();
                        $(".resUrl", thisBox).attr("href", "/app/public/files/"+json.fname);
                    }

                    $(".btnSwitch", thisBox).removeClass('disabled').toggle();
                    prgrsBar.hide();
                    $("p", dropZone).removeClass('text-danger').addClass('text-success').html('<span class="label label-success"> Ok! </span>');
                    $("input[type=file]", thisBox).show();
                } else if(json.error){
                    prgrsBar.removeClass('progress-info').addClass('progress-danger');
                    $("p", dropZone).removeClass('text-success').addClass('text-danger').html('<span class="label label-danger">Error</span> '+json.error);
                    $("input[type=file]", thisBox).show();
                }
            },
            error: function(){
                prgrsBar.removeClass('progress-info').addClass('progress-danger');
                $("p", dropZone).removeClass('text-success').addClass('text-danger').html('<span class="label label-danger">Error</span> Unaxpected Error :(');
                $("input[type=file]", thisBox).show();
             }
         });
     }
}

/**
 * Check the file progres with session 
 * 
 * @param {int} prgrsId
 * @param {obj/dom} thisBox
 *
 * @return {json}
 */
function fileCheckProgress(prgrsId, thisBox){
    var message, timer = -1, prgrsBar = $('.progressBar', thisBox), counter = 0;
    $(".bar", prgrsBar).attr('style', 'width: 0%;');
    prgrsBar.removeClass('progress-danger').addClass('progress-info').show();
 
	timer = setInterval(function(){
		counter++;
		if(counter > 15){
			clearInterval(timer);
			prgrsBar.removeClass('progress-info').addClass('progress-danger');
			$("p", thisBox).addClass('text-danger').html('<span class="label label-danger">Error</span> File non caricato');
		}
		
		
		message = $.parseJSON($("iframe", thisBox).contents().find("pre").text());
		if(message.files[0].name.length > 0){
			var thisBoxPrnt = thisBox.parent().parent();
			clearInterval(timer);
			
			prgrsBar.hide();
			$("p", thisBox).removeClass('text-danger').addClass('text-success').html('<span class="label label-success">Ok! </span>');
			
			setTimeout(function(){
				$(".divSwitch", thisBoxPrnt).toggle();
				$(".btnSwitch", thisBoxPrnt).removeClass('disabled').toggle();
				$(".resUrl", thisBoxPrnt).attr("src", "/app/public/files/property/"+message.files[0].name);
			}, 3000);
		}
	}, 3000);
	/*
    timer = setInterval(function(){
        $.ajax({
            url: '/app/private/progress.php',
            //cache: false,
            method: 'POST',
            dataType: 'json',
            data: {prgrsId: prgrsId},
            success: function(json){
                if(json.percent)
                    $(".bar", prgrsBar).attr('style', 'width:'+json.percent+'%;');
				
				if(json.status != 1){
                    if(file_for_finish > 5){
                        clearInterval(timer);
                        prgrsBar.removeClass('progress-info').addClass('progress-danger');
                        $("p", thisBox).addClass('text-danger').html('<span class="label label-danger">Error</span> Unexpected error');
                        file_for_finish = 0;
                    }
                    file_for_finish++;
                    message = $.parseJSON($("iframe", thisBox).contents().find("#message").text());
                    if(message.success){
                        var thisBoxPrnt = thisBox.parent().parent();
                        clearInterval(timer);
                        
                        prgrsBar.hide();
                        $("p", thisBox).removeClass('text-danger').addClass('text-success').html('<span class="label label-success">Ok! </span> '+message.success);
                        
                        setTimeout(function(){
                            $(".divSwitch", thisBoxPrnt).toggle();
                            $(".btnSwitch", thisBoxPrnt).removeClass('disabled').toggle();
                            $(".resUrl", thisBoxPrnt).attr("src", "/app/public/files/"+message.fname);
                        }, 3000);
                    } else if(message.error){
                        clearInterval(timer);
                        prgrsBar.removeClass('progress-info').addClass('progress-danger');
                        $("p", thisBox).addClass('text-danger').html('<span class="label label-danger">Error</span> '+message.error);
                    }
                }				
            },
            error: function(){
                $("p", thisBox).addClass('text-danger').html('<span class="label label-danger">Error</span> Unexpected error');
            }
        });
    }, 2500);*/
}


/*-*-*-*-*-    File Upload Func     *-*-*-*-*
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-END-*/