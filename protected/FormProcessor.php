<?php
abstract class FormProcessor {    
    // I metodi che aiutano a processare le form
    // questa classe puo essere riutilizzata ovunque creeremo delle form
    var $tags = "<a><img><b><strong><em><i><ul><li><ol><p><br>";


    public function cleanHtml($html){
        $tmp = $html;

        //$html = strip_tags($html,$tags);

        while(true) {
                // tutto quello chenon va bene lo sostituiamo
                //$string = preg_replace ('/<[^>]*>/', ' ', $string); // elimina tutti i tag html e php
                $html = preg_replace('/(<[^>]*)javascript:([^>]*>)/i', ' ', $html); // elimina tag e javascript !!!

                // se html non cambia nel preg_raplace, out da qua
                if ($html == $tmp)
                        break;

                $tmp = $html;
        }

        // togliamo gli acapi, tabulazioni e nuove linee
        $string = str_replace("\r", '', $html);
        $string = str_replace("\n", ' ', $string);
        $string = str_replace("\t", ' ', $string);

        // togliamo gli spazi multipli
        $string = trim(preg_replace('/ {2,}/', ' ', $string));

        return $string;
    }   
    
    
    
    
    
    
    /* MORE */
    
    
    protected $_errors = array();
    protected $_vals = array();
    
    public function __construct()
    {

    }
    
    abstract function process($data);  
    
    public function sanitize($string){        
        $string = strip_tags($string);
        
        return $string;
    }


    public function addError($key, $val)
    {
        if (array_key_exists($key, $this->_errors)) {
            if (!is_array($this->_errors[$key]))
                $this->_errors[$key] = array($this->_errors[$key]);

            $this->_errors[$key][] = $val;
        }
        else
            $this->_errors[$key] = $val;
    }

    public function getError($key)
    {
        if ($this->hasError($key))
            return $this->_errors[$key];

        return null;
    }

    public function getErrors()
    {
        return $this->_errors;
    }

    public function hasError($key = null)
    {
        if (strlen($key) == 0)
            return count($this->_errors) > 0;

        return array_key_exists($key, $this->_errors);
    }
        
        
    public function __set($name, $value)
    {
        $this->_vals[$name] = $value;
    }

    public function __get($name)
    {
        return array_key_exists($name, $this->_vals) ? $this->_vals[$name] : null;
    }	
}
?>