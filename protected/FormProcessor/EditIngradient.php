<?php
class FormProcessor_EditIngradient extends FormProcessor
{
    protected $db = null;
    public $_item = null;
    public $success_message = 'Ingradiente Modificato con successo';

    public function __construct($db, $item_id)
    {
        parent::__construct();
        
        $this->_item = new Model_Ingradient($db, $item_id, true);
        $this->name = $this->_item->name;
        $this->brand = $this->_item->brand;
        $this->label = $this->_item->label;
    }

    public function process($request)
    {           
        // Nome
        $this->name = $this->sanitize($request['name']);
		if(strlen($this->name) == 0){
			$this->addError('name', 'Per favore inserisci un nome valido');
        } elseif (strlen($this->name) > 255) {
            $this->addError('name', 'Per favore inserisci un nome valido');
        } else {
            $this->_item->name = $this->name;
        }
        
        if(!$this->hasError()){
            $this->_item->save();
        }
        
        return !$this->hasError();
    }
}
?>