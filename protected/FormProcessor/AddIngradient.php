<?php
class FormProcessor_AddIngradient extends FormProcessor
{
    protected $db = null;
    public $_item = null;
    public $success_message = 'Ingradiente Aggiunto con successo';

    public function __construct($db)
    {
        parent::__construct();
        
        $this->_item = new Model_Ingradient($db);
    }

    public function process($request)
    {           
        // Nome
        $this->name = $this->sanitize($request['name']);
		if(strlen($this->name) == 0){
			$this->addError('name', 'Per favore inserisci un nome valido');
        } elseif (strlen($this->name) > 255) {
            $this->addError('name', 'Per favore inserisci un nome valido');
        } else {
            $this->_item->name = $this->name;
        }
        
        if(!$this->hasError()){
            $this->_item->save();
        }
        
        return !$this->hasError();
    }
}
?>