<?php
class FormProcessor_AddProduct extends FormProcessor
{
    protected $db = null;
    public $_item = null;
    public $success_message = 'Prodotto Aggiunto con successo';

    public function __construct($db)
    {
        parent::__construct();
        
        $this->_item = new Model_Product($db);
    }

    public function process($request)
    {           
        // Nome
        $this->name = $this->sanitize($request['name']);
		if(strlen($this->name) == 0){
			$this->addError('name', 'Per favore inserisci un nome valido');
        } elseif (strlen($this->name) > 255) {
            $this->addError('name', 'Per favore inserisci un nome valido');
        } else {
            $this->_item->name = $this->name;
        }
        
        // brand
        $this->brand = $this->sanitize($request['brand']);
		if(strlen($this->brand) == 0){
			$this->addError('brand', 'Per favore inserisci un brand valido');
        } elseif (strlen($this->brand) > 255) {
            $this->addError('brand', 'Per favore inserisci un brand valido');
        } else {
            $this->_item->brand = $this->brand;
        }
        
		// label - ingradienti
        $this->label = $this->sanitize($request['label']);
        $lenght = strlen($this->label);
        if($lenght < 5){
            $this->addError('label', 'Cosi pochi ingradienti? Sei shure?');
        } elseif($lenght > 3000 ) {
            $this->addError('label', 'Cosi tanti ingradienti, parliamone info@edotest.it');
        } else {
            $this->_item->label = $this->label;
        }
        
        if(!$this->hasError()){
            $this->_item->save();
        }
        
        return !$this->hasError();
    }
}
?>