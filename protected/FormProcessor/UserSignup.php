<?php
class FormProcessor_UserSignup extends FormProcessor {
    protected $db = null;
    public $user = null;
    public $time_zone;
    public $language;
    protected $_validateOnly = false;
    protected $validateMail = null;

    public function __construct($db)
    {
        parent::__construct();

        $this->db = $db;
        $this->user = new Model_User($db);
        $this->user->type = 'user';
    }

    public function process($data)
    {
        // * validate the user's name
        $this->first_name = $this->sanitize($data['first_name']);
        if(strlen($this->first_name) == 0)
            $this->addError('first_name', 'Per favore inserite il vostro nome');
        else
            $this->user->first_name = $this->first_name;

        $this->last_name = $this->sanitize($data['last_name']);
        if(strlen($this->last_name) == 0)
            $this->addError('last_name', 'Per favore inserite il vostro cognome');
        else
            $this->user->last_name = $this->last_name;



        // * validate the e-mail address
        $this->mail = $this->sanitize($data['mail']);

        if(strlen($this->mail) == 0)
            $this->addError('mail', 'Per favore inserite il vostro indirizzo e-mail');
        elseif(!filter_var($this->mail, FILTER_VALIDATE_EMAIL))
            $this->addError('mail', 'Per favore inserite un indirizzo e-mail valido');
        //elseif($this->user->mailExist($this->mail))
        //    $this->addError('mail', 'Con questa mail un utente e gia registrato');
        else
            $this->user->mail = $this->mail;



        // * Password validation
        $this->password = $this->sanitize($data['password']);
        $this->password_confirm = $this->sanitize($data['password_confirm']);
        
        if(!preg_match('/^(?=.*\d)(?!.*\s).{4,8}$/', $this->password))
            $this->addError('password', 'La password deve contenere 6 o piú caratteri e contenere un numero');
        elseif($this->password != $this->password_confirm)
            $this->addError('password_confirm', 'Attenzione! Hai inserito due password diverse');
        else
            $this->user->password = $this->password;


        /* validate reCAPTCHA
        require_once(ROOT.DS.'private'.DS.'libraries'.DS.'ReCaptcha'.DS.'recaptchalib.php');
        $this->private_key = Zend_Registry::get('config')->captcha->private_key;
        $resp = recaptcha_check_answer ($this->private_key,	$_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

        if(!$resp->is_valid)
            $this->addError('captcha', 'Il codice di sicurezza CAPTCHA non è valido');
        */

        $this->user->active = 0;
        //$this->user->ts_created = time('YYYY-MM-dd HH:mm:ss');


        // if no errors have occurred, save the user
        if(!$this->hasError()) {
            //$this->user->save();
            $this->user->create();
        }

        // return true if no errors have occurred
        return !$this->hasError();
    }
}
?>