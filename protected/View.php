<?php
/** Artur Mamedov - arturmamedov1993@gmail.com
 * view.php
 * Handles the view functionality of framework
**/
class View {
    /**
     * Smarty object
     * @var object
     */
    public $smarty;
    /**
     * Holds variables assigned to template
     * @var array 
     */
    private $data = array();
    /**
     * Extension of file
     * @var srting 
     */
    public $ext = '.tpl';
    /**
     * File to display
     * @var srting 
     */
    public $file = 'index';
    /**
     * Path where are file to display
     * @var srting 
     */
    public $path = 'index';
    /**
     * Use smarty or no
     * @todo ever yes and remove the possibility to change
     * @var boolean
     */
    public $vsmarty = true;
    /**
     * Holds render status of view.
     * @var object 
     */
    private $render = FALSE;

    
    
    /**
     * Accept a template to load and change from smarty to normal .php view
     * 
     * @param boolean $smarty
     * @param string $ext
     * @param string $folder
     * @param string $template
     */ 
    public function __construct($folder = 'index', $template = 'index', $vsmarty = true){
        $this->vsmarty = $vsmarty;
        
        if($this->vsmarty){
            // Smarty initialize
            require_once ROOT.DS.'protected'.DS.'libraries'.DS.'Smarty'.DS.'libs'.DS.'Smarty.class.php';
            $this->smarty = new Smarty();

            // Smarty dirPath
            $this->smarty->setTemplateDir(ROOT.DS.'protected'.DS.'Views');
            $this->smarty->setCompileDir(ROOT.DS.'protected'.DS.'data'.DS.'templates_c');
            $this->smarty->setCacheDir(ROOT.DS.'protected'.DS.'data'.DS.'cache');
        } else {
            //compose file name
            $file = ROOT.DS.'protected'.DS.'Views'.DS.$folder.DS.$template.$this->ext;

            if (file_exists($file)) {
                /**
                 * trigger render to include file when this model is destroyed
                 * if we render it now, we wouldn't be able to assign variables
                 * to the view!
                **/
                $this->render = $file;
            }
        }
    }
    

    
// ------  Smarty View -------- //    
    
    
    /*
    public function view(){
        
        if($this->debug)
            $this->smarty->debugging = true;
    }
    */
    
    
    /**
     * When writing data to inaccessible properties. 
     * assign it to smarty view
     * 
     * @param type $name
     * @param type $value
     */
    public function __set($name, $value){
        $this->smarty->assign($name, $value); 
    }
    
    /**
     * When get data from inaccessible properties. 
     * return null or data if isset
     * 
     * @param type $name
     */
    public function __get($name){
        return ($this->smarty->getTemplateVars($name)) ? $this->smarty->getTemplateVars($name) : NULL;
    }
    
    /**
     * After Controller function execute display that setted 
     * @depends Router.php
     * default: in Router.php or index/index.tpl
     */
    public function display(){
        $view = $this->path.'/'.$this->file.$this->ext;
       
        $this->smarty->display($view);
    }




// ------  Normal View -------- //
    
    

    /**
     *  Receives assignments from controller and stores in local data array
     * 
     * @param string $variable
     * @param mixed $value
     */
    public function assign($variable , $value) {
        $this->data[$variable] = $value;
    }
    
    /**
     * 
     * @param string $folder
     * @param string $template
     */
    public function setView($folder, $template, $ext = '.php'){
        $this->ext = $ext;
        
        $file = ROOT.DS.'protected'.DS.'Views'.DS.strtolower($folder).DS.strtolower($template).$this->ext;
    
        if(file_exists($file)) {
            $this->render = $file;
        }
    }

    /**
     * Render the output directly to the page, or optionally, return the
     * generated output to caller.
     * 
     * @param $direct_output Set to any non-TRUE value to have the 
     * output returned rather than displayed directly.
    **/
    public function render($direct_output = TRUE)
    {
        // Turn output buffering on, capturing all output
        if($direct_output !== TRUE) {
            ob_start();
        }

        // Parse data variables into local variables
        $data = $this->data;
    
        // Get template
        require_once($this->render);
        
        // Get the contents of the buffer and return it
        if($direct_output !== TRUE) {
            return ob_get_clean();
        }
    }
} 