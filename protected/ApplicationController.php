<?php
/**
 * General Controller of applications
 * define a gloabal proprieties and methods
 */
class ApplicationController{
    /**
     * Database of Application
     * @var Object
     */
    public $db;
    /**
     * View of Application
     * @var Object
     */
    public $view;
    /**
     * GET request array
     * @var array
     */
    public $_get;
    /**
     * identity of user for get hime logged
     * @var object
     */
    public $identity;
    /**
     * state logged or no of user
     * @var boolean
     */
    public $authenticated;
    public $tsn = ' | '.SITE_NAME;
	
	public function _init(){        
        $this->view->tsn = $this->tsn;
        
        // messaggi di notifica alla vista se ci sono
        if(isset($_SESSION['message'])){
            $this->view->message = $_SESSION['message'];
            $this->view->message_type = $_SESSION['message_type'];
            $_SESSION['message'] = NULL;
            $_SESSION['message_type'] = NULL;
        }
	}
    
    public function __construct(){
        //require_once dirname(dirname(ROOT)).DS.'mysqli-lightLib'.DS.'mysqli.php';
        $this->db = new Database_Mysqli(DB_HOST, DB_USER, DB_PSW, DB_NAME);
        $this->view = new View('index', 'index');
        
        if(isset($_SESSION['identity'])){
            $this->authenticated = $this->view->authenticated = true;
            $this->identity = $this->view->identity = $_SESSION['identity'];
        } else {
            $this->authenticated = $this->view->authenticated = false;
        }
    }
    
    /**
     * Strip tags
     * 
     * @param string $string
     * 
     * @return string stripped string
     */
    public function sanitize($string){        
        $string = strip_tags($string);
        // to do .. another sanitize method
        return $string;
    }
}
