<?php
/**
 * Abstract class used to easily manipulate data in a database table
 * via simple load/save/delete methods
 */
abstract class DataObject
{
    const TYPE_TIMESTAMP = 1;
    const TYPE_BOOLEAN   = 2;

    protected static $types = array(self::TYPE_TIMESTAMP, self::TYPE_BOOLEAN);

    protected $_id = null;
    private $_properties = array();

    protected $_db = null;
    protected $_table = '';
    protected $_idField = '';

    /**
     * Construct the object with database connection,table and id_field
     * 
     * @param object $db
     * @param string $table
     * @param string $idField
     */
    public function __construct($db, $table, $idField){
        $this->_db = $db;
        $this->_table = $table;
        $this->_idField = $idField;
    }

    /**
     * Load and set data for object
     * @depends $this->_load(), $this->_init()
     * 
     * @param int/string $id
     * @param string $field if passed load by the field else from $this->_idField
     * 
     * @return boolean/object false or data
     */
    public function load($id, $field = null){
        if (strlen($field) == 0)
            $field = $this->_idField;

        if ($field == $this->_idField){
            $id = (int) $id;
            if ($id <= 0)
                return false;
        }

        $query = sprintf('SELECT %s FROM %s WHERE %s = %d', join(', ', $this->getSelectFields()), $this->_table, $field, $id);

        return $this->_load($query);
    }

    /**
     * Get the fields that present in this created object
     * 
     * @param string $prefix before any name ex: user_ => user_name
     * 
     * @return string
     */
    protected function getSelectFields($prefix = ''){
        $fields = array($prefix . $this->_idField);
        foreach ($this->_properties as $k => $v)
            $fields[] = $prefix . $k;

        return $fields;
    }

    /**
     * Load/Retrive data from db and pass him to $this->_init()
     * 
     * @param string $query
     * 
     * @return boolean false if no retrive,true
     */
    protected function _load($query){
        $this->_db->query($query);
        $row = $this->_db->fetch();
        if (!$row)
            return false;

        $this->_init($row);

        $this->postLoad();

        return true;
    }

    /**
     * Set the $this->_properties of object with db value
     * 
     * @param object/array $row mysql object of data
     */
    public function _init($row){
        foreach ($this->_properties as $k => $v){
            $val = $row[$k];

            switch ($v['type']){
                case self::TYPE_TIMESTAMP:
                    if (!is_null($val))
                        $val = strtotime($val);
                    break;
                case self::TYPE_BOOLEAN:
                    $val = (bool) $val;
                    break;
            }

            $this->_properties[$k]['value'] = $val;
        }
        $this->_id = (int) $row[$this->_idField];
    }

    /**
     * Return all interested in transiction _properties as string
     * ex: user_id => 295, item_id => 345
     * 
     * @param string $sep (default: '=>')
     *
     * @return string
     */
    public function propertiesToString($sep = '=>'){
        $string_row = '';

        foreach($this->_properties as $k => $v) {
            if(@$update && !$v['updated'])
                continue;

            switch ($v['type']) {
                case self::TYPE_TIMESTAMP:
                    if (!is_null($v['value'])) {
                        if ($this->_db instanceof Zend_Db_Adapter_Pdo_Pgsql)
                            $v['value'] = date('Y-m-d H:i:sO', $v['value']);
                        else
                            $v['value'] = date('Y-m-d H:i:s', $v['value']);
                    }
                    break;

                case self::TYPE_BOOLEAN:
                    $v['value'] = (int) ((bool) $v['value']);
                    break;
            }

            $string_row .= $k.' '.$sep.' '.$v['value'].', ';
        }

        return $string_row;
    }


    /**
     * Save or update a record in table of created object
     * 
     * @param boolean $useTransactions
     * @return boolean
     */
    public function save($useTransactions = true){
        $update = $this->isSaved();

        if ($useTransactions)
            $this->_db->_beginTransaction();

        if ($update)
            $commit = $this->preUpdate();
        else
            $commit = $this->preInsert();

        if (!$commit) {
            if ($useTransactions)
                $this->_db->_rollback();
            return false;
        }

        $row = array();

        foreach ($this->_properties as $k => $v){
            if ($update && !$v['updated'])
                continue;

            switch ($v['type']) {
                case self::TYPE_TIMESTAMP:
                    if (!is_null($v['value'])) {
                        if($this->_db instanceof Zend_Db_Adapter_Pdo_Pgsql)
                            $v['value'] = date('Y-m-d H:i:sO', $v['value']);
                        else
                            $v['value'] = date('Y-m-d H:i:s', $v['value']);
                    }
                    break;

                case self::TYPE_BOOLEAN:
                    $v['value'] = (int) ((bool) $v['value']);
                    break;
            }

            $row[$k] = $v['value'];
        }
        
        if (count($row) > 0) {
            // perform insert/update
            if ($update) {
                $this->_db->update($this->_table, $row, sprintf('%s = %d', $this->_idField, $this->getId()));
            }
            else {
                $this->_db->insert($this->_table, $row);
                $this->_id = $this->_db->lastInsertId($this->_table, $this->_idField);
            }
        }

        // update internal id

        if ($commit) {
            if ($update)
                $commit = $this->postUpdate();
            else
                $commit = $this->postInsert();
        }

        if ($useTransactions) {
            if ($commit)
                $this->_db->_commit();
            else
                $this->_db->_rollback();
        }

        return $commit;
    }

    /**
     * Delete db data of $this object
     * 
     * @param bollean $useTransactions with InnoDB
     * 
     * @return boolean
     */
    public function delete($useTransactions = true)
    {
        if(!$this->isSaved())
            return false;

        if($useTransactions)
            $this->_db->_beginTransaction();

        $commit = $this->preDelete();

        if($commit){
            $this->_db->delete($this->_table, sprintf('%s = %d', $this->_idField, $this->getId()));
        } else {
            if($useTransactions)
                $this->_db->_rollback();
            return false;
        }

        $commit = $this->postDelete();

        $this->_id = null;

        if($useTransactions){
            if($commit)
                $this->_db->_commit();
            else
                $this->_db->_rollback();
        }

        return $commit;
    }

    /**
     * Are this data yet saved in db
     * @return boolean $this->_id > 0;
     */
    public function isSaved(){
        return ($this->getId() > 0)?true:false;
    }

    /**
     * Return $this->_id or 0 if not exist
     * 
     * @return int
     */
    public function getId(){
        return (int) $this->_id;
    }

    /**
     * Get $this->_db object
     * 
     * @return object
     */
    public function getDb(){
        return $this->_db;
    }

    /**
     * when code set a undefined proprieties add him to $this->_proprieties and set updated
     * 
     * @param string $name
     * @param string $value
     * 
     * @return boolean
     */
    public function __set($name, $value){
        if(array_key_exists($name, $this->_properties)) {
            $this->_properties[$name]['value'] = $value;
            $this->_properties[$name]['updated'] = true;
            return true;
        }

        return false;
    }

    /**
     * when code try to get undefined proprieties return null
     * 
     * @param string $name
     * 
     * @return NULL/mixed or _proprieties or NULL
     */
    public function __get($name){
        return array_key_exists($name, $this->_properties) ? $this->_properties[$name]['value'] : null;
    }

    /**
     * Process for $this->add() a value to $this->_proprieties
     * 
     * @param string $field name of _proprieties
     * @param mixed $default default value or sett its after with load() in example
     * @param string $type es: TIMESTAMP, DATETIME
     */
    protected function add($field, $default = null, $type = null){
        $this->_properties[$field] = array(
            'value'   => $default,
            'type'    => in_array($type, self::$types) ? $type : null,
            'updated' => false);
    }

    /**
     * method called before database insert 
     * @return boolean
     */
    protected function preInsert(){
        return true;
    }

    /**
     * method called after database insert
     * @return boolean
     */
    protected function postInsert(){
        return true;
    }

    /**
     * method called before database update
     * @return boolean
     */
    protected function preUpdate(){
        return true;
    }

    /**
     * method called after n database update
     * @return boolean
     */
    protected function postUpdate(){
        return true;
    }

    /**
     * method called before a database delete
     * @return boolean
     */
    protected function preDelete(){
        return true;
    }

    /**
     * method called after an database delete
     * @return boolean
     */
    protected function postDelete(){
        return true;
    }

    /**
     * after data load from database
     * @return boolean
     */
    protected function postLoad(){
        return true;
    }

    /**
     * For a mysql $data of more rows _init() for create many data objects 
     * @depends $this->_init(), $this->getId()
     * 
     * @param object $db
     * @param string $class
     * @param array/object $data mysql data
     * 
     * @return array with data objects
     * 
     * @throws Exception if the passed data objects not extended from here
     */
    public static function BuildMultiple($db, $class, $data){
        $ret = array();

        if (!class_exists($class))
            throw new Exception('Undefined class specified: ' . $class);

        $testObj = new $class($db);

        if(!$testObj instanceof DataObject)
            throw new Exception('Class does not extend from DataObject');

        foreach($data as $row){
            $obj = new $class($db);
            $obj->_init($row);

            $ret[$obj->getId()] = $obj;
        }

        return $ret;
    }
}
?>