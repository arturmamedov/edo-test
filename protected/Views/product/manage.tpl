{include file='layout/head.tpl' close=false}
<style>{literal}
    #products {min-height: 200px;}
    .ping {
        font-size: 12px;
    }
    #editProduct {
        font-size: 14px;
    }
    #editProduct {border-left: 1px solid #FFFFFF;}
    #editProduct .form-group {border-bottom: 1px solid #FFFFFF;}
{/literal}</style>
</head>
<body>
{include file='layout/navbar.tpl'}
    
<div class="container">
    <div class="col-sm-10 col-sm-offset-1 jumbotron">
		<div class="page-header no-margin">
            <h2 class="text-center margin-top-30 no-margin">Gestisci Prodotti</h2>
        </div>
        
        <div class="row">
            <div class="">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for..." id="searchProduct">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        
        <ul class="list-group margin-top-30 col-sm-6" id="products">
        {foreach from=$products item=p}
            <li class="list-group-item product">
                <small class="pull-left badge bg-success pid">#{$p->id}</small>
                <h3 class="text-center">{$p->name} - <span class="text-danger">{$p->brand}</span></h3>
                <p><small class="ping">
                    Ingradienti:
                    <strong>{$p->label}</strong>
                </small><p>
                <div class="clearfix"></div>
                <div class="pull-right">
                    <a href="javascript:;" class="edit" data-id="{$p->id}">Modifica</a>
                    &nbsp;
                    <a href="/product/delete/{$p->id}" class="delete text-danger">Elimina</a> 
                </div>
                <div class="clearfix"></div>
            </li>
        {foreachelse}
            <h3 class="text-center">Nessun prodotto nel database ... <a href="/product/add">Inserisci il primo</a></h3>
        {/foreach}
        </ul>
        
        <div class="margin-top-30 col-sm-6 display-none" id="editProduct">
            <div class="form-group row">
                <label class="col-xs-12 col-sm-4 control-label col-sm" for="id">Id</label>
                <div class="col-xs-12 col-sm-8">
                    <input id="id" class="form-control" name="id" value="#" type="text" placeholder="Inserisci il nome del prodotto" disabled />
                    <span class="help-block"></span>
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-xs-12 col-sm-4 control-label col-sm" for="name">Nome Prodotto</label>
                <div class="col-xs-12 col-sm-8">
                    <input id="name" class="form-control" name="name" value="" type="text" placeholder="Inserisci il nome del prodotto" />
                    <span class="help-block"></span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-xs-12 col-sm-4 control-label " for="brand">Brand</label>
                <div class="col-xs-12 col-sm-8 ">
                    <input id="brand" class="form-control" name="brand" value="" type="text" placeholder="Inserisci nome del brand" />
                    <span class="help-block"></span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-xs-12 col-sm-4 control-label " for="label">Ingradienti <span title='* Separati da "punto", "virgola" o "punto e virgola"
                        "due punti" o "parentesi tonda" per unire "padre" con quella "figlia"' class="withTt">(?)</span></label>
                <div class="col-xs-12 col-sm-8 ">
                    <textarea id="label" class="form-control" placeholder="Inserisci gli ingradienti *" name="label" rows="5" disabled></textarea>
                    <span class="help-block"></span>
                </div>
                
                <div class="row text-center">
                    <p><a href="javascript:;" class="btn btn-primary extract">Estrai Ingradienti</a></p>
                </div>
            </div>
            
            <div class="row">
                <h3>Ingradienti estratti:</h3>
                
                <ul class="list-unstyled list-group" id="ingradients">
                    
                </ul>
            </div>
            
            <a href="#" class="fullEdit pull-right">Modifica prodotto intero</a>
        </div>
  	</div><!--/col-->
</div>

{include file='layout/footer.tpl' hide=true}

<li class="list-group-item display-none product" id="productClone">
    <small class="pull-left badge bg-success pid">#{$p->id}</small>
    <h3 class="text-center"> - <span class="text-danger"></span></h3>
    <p><small class="ping">
        Ingradienti:
        <strong></strong>
    </small><p>
    <div class="clearfix"></div>
    <div class="pull-right">
        <a href="javascript:;" class="edit" data-id="">Modifica</a>
        &nbsp;
        <a href="/product/delete/" class="delete text-danger">Elimina</a> 
    </div>
    <div class="clearfix"></div>
</li>

<li id="ingradientClone" class="list-group-item display-none ingradient">
    <span class="ing"></span>
    <div class="pull-right added display-none">
        <a href="javascript:;" class="btn related"><i class="glyphicon glyphicon-check"></i></a>
        <a href="javascript:;" class="btn btn-success remove"><i class="glyphicon glyphicon-ok"></i></a>
        <a href="javascript:;" class="btn btn-danger remove"><i class="glyphicon glyphicon-trash"></i></a>
    </div>
    <div class="pull-right notadded display-none">
        <a href="javascript:;" class="btn related"><i class="glyphicon glyphicon-check"></i></a>
        <a href="javascript:;" class="btn btn-default add"><i class="glyphicon glyphicon-ok"></i></a>
        <a href="javascript:;" class="btn btn-danger display-none"><i class="glyphicon glyphicon-trash"></i></a>
    </div>
    <div class="clearfix"></div>
</li>
{include file='layout/foot.tpl'}

<!-- JavaScript jQuery-->
{literal}
<script type='text/javascript'>
$(document).ready(function() {
    $(".withTt").tooltip();
    
    var autocTimer;
    $("#searchProduct").on('keydown', function(){
        var term = $(this).val();
        
        if(term.length >= 3) {
            //autocTimer = setTimeout(function(){
                searchProduct(term);
            //}, 1500);
        }     
    });

    /**
     * Function for search
     * @param {string} term
     * @returns {nothing}
     */
    function searchProduct(term){
        clearSearch();
        
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/product/search',
            data: {term: term},
            cache: 'false',
            timeout: 20000,
            success: function(json){
                $('#products').html('');
                
                if(json.success){
                    for(var i in json.products){
                        var p = json.products[i];
                        
                        var product = $("#productClone").clone();
                        product.attr('id', p.id);
                        product.find('.pid').text('#'+p.id);
                        product.find('h3').html(p.name+' - <span class="text-danger">'+p.brand+'</span>');
                        product.find('.ping').html('Ingradienti: <strong>'+p.label+'</strong>');
                        product.find('.edit').attr('data-id', p.id);
                        product.find('.delete').attr('href', '/product/delete/'+p.id);
                        
                        product.appendTo('#products');
                        product.removeClass('display-none');
                    }
                } else if(json.error) {
                    // todo ..
                }               
            },
            error: function(){
                // todo
            }
        });
    }
    
    /**
    * For clear suggestion space
    */
   function clearSearch(){
       $("#products").html('<h4 class="text-center">... Ricerca, Attendi ...</h4>');
       $("#editProduct").addClass('display-none');
   }
   
   /**
    * Evento click sul bottone modifica
    * N.B. E' la prima volta che uso la tattica del far tornare il risultato Ajax in quel modo (con async: false), di solito eseguivo il codice *sottostante* direttamente nel success di ajax, ma volevo provare a rendere il codice piu' riutilizabile tenendo getProduct(id); con un solo compito, return dati del prodotto.
    * 
    * 
    */
   $("#products").on('click', '.edit', function(){
       $("ul#ingradients").addClass('display-none');
       
       $("#products .list-group-item").removeClass('list-group-item-success');
       var id = parseInt($(this).data('id'));
       $(this).parent().parent().addClass('list-group-item-success');
       
       var json_product = getProduct(id);
       var product = json_product.product[0]
       
       // *sottostante*
       if(json_product.success == true){
            $("#editProduct").find('input[name=id]').val(product.id);
            $("#editProduct").find('input[name=name]').val(product.name);
            $("#editProduct").find('input[name=brand]').val(product.brand);
            $("#editProduct").find('textarea[name=label]').val(product.label);
            
            $("#editProduct").find('.fullEdit').attr('href', '/product/edit/'+product.id);

            $("#editProduct").removeClass('display-none');
        }
   });
   
   /**
     * Function for get one product
     * 
     * @param {integer} id
     * 
     * @returns {json|Boolean} product data from server
     */
    function getProduct(id)
    {
        var result = false;
        
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/product/get/'+id,
            cache: 'false',
            timeout: 20000,
            async: false,
            success: function(json){
                result = json;           
            },
            error: function(){
                // todo
                result = false;
            }
        });
        
        return result;
    }
    
    /**
     * Evento estrai ingradienti
     * 1 - Leggi textarea[disabled] e recupera i dati
     * 2 - Invia i dati sul server dove:
     *  - RegExp per leggerli e separarli, io preferisco far lavorare i server sui dati sensibili i complicati, per non impegnare il client
     *  - Controllo se gli ingradienti sono gia presenti o meno nel DB per poi mostrarli
     *  3 - Genero HTML appunto per mostrarli e dare opzioni
     *  @author #withArtur
     */
    $("#editProduct").on('click', '.extract', function(){
        $("ul#ingradients").removeClass('display-none');
        var ingradients = $("#editProduct textarea[name=label]").val();
            
        // recupera e visualizza gli ingradienti
        getIng(ingradients);
    });
    
    /**
     * Mostriamo gli ingradienti e diamo le opzioni da svolgerci
     * *ecco di solito faccio cosi,async: false come nel getProduct()
     *  fa aspettare il client e mi sa che non va bene*
     * 
     * @param {string} ing
     * 
     * @returns {nothing}
     */
    function getIng(ing)
    {
        clearIngslist();
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ingradient/get/',
            data: {ingradients: ing, product_id: $("#editProduct input[name=id]").val()},
            cache: 'false',
            timeout: 20000,
            success: function(json){
                $('ul#ingradients').html('');
                
                if(json.success == true){
                    for(var key in json.ings){
                        var ing = json.ings[key];
                        var newIng = $("#ingradientClone").clone(), hkey = parseInt(key) + 1;
                        newIng.prepend(hkey+' - ').find(".ing").text(ing.name);
                        newIng.attr('id', 'ing-'+ing.name);
                        
                        // in database?
                        if(ing.saved > 0){
                            newIng.find(".added").removeClass('display-none');
                            newIng.attr('id', ing.saved);
                        } else {
                            newIng.find(".notadded").removeClass('display-none');
                        }
                        
                        // relatianal to product?
                        if(ing.inproduct){
                            newIng.find(".related").addClass('btn-success');
                        } else {
                            newIng.find(".related").addClass('btn-warning');
                        }

                        newIng.appendTo('ul#ingradients');
                        newIng.removeClass('display-none');
                    }
                }    
            },
            error: function(){
                // todo
            }
        });
    }
    
    /**
     * For clear suggestion space
     */
    function clearIngslist(){
       $("ul#ingradients").html('<h4 class="text-center">... Carico ...</h4>');
    }
    
    $("ul#ingradients").on('click', 'a.related', function(){
        var ingradient_id = $(this).parent().parent().attr('id'), 
            ingradient_name = $(this).parent().parent().find('.ing').text(), 
            product_id = $("#editProduct input[name=id]").val();

        createRelation(product_id, ingradient_id, ingradient_name);
    });
    
    function createRelation(product_id, ingradient_id, ingradient_name)
    {   
        $.ajax({
            type: 'POST',
            data: {ingradient_id:ingradient_id, product_id:product_id, ingradient_name: ingradient_name},
            url: '/ingradient/createrelation',
            dataType: 'json',
            success: function(json){
                if(json.success == true){
                    var ingradients = $("#editProduct textarea[name=label]").val();

                    // recupera e visualizza gli ingradienti
                    getIng(ingradients);
                }
            },
            error: function(){
                // todo
            }
        });
    }
    
    
    $("ul#ingradients").on('click', 'a.add', function(){
        var ing = $(this).parent().parent().find('.ing').text();

        addIngradient(ing);
    });
    
    function addIngradient(ing)
    {   
        $.ajax({
            type: 'POST',
            data: {name: ing},
            url: '/ingradient/addjson',
            dataType: 'json',
            success: function(json){
                if(json.success == true){
                    var ingradients = $("#editProduct textarea[name=label]").val();

                    // recupera e visualizza gli ingradienti
                    getIng(ingradients);
                }
            },
            error: function(){
                // todo
            }
        });
    }
    
    $("ul#ingradients").on('click', 'a.remove', function(){
        var id = $(this).parent().parent().attr('id');

        removeIngradient(id);
    });
        
    function removeIngradient(id)
    {   
        $.ajax({
            type: 'GET',
            url: '/ingradient/removejson',
            dataType: 'json',
            data: {ingradient_id: id, product_id: $("#editProduct input[name=id]").val()},
            success: function(json){
                if(json.success == true){
                    var ingradients = $("#editProduct textarea[name=label]").val();

                    // recupera e visualizza gli ingradienti
                    getIng(ingradients);
                }
            },
            error: function(){
                // todo
            }
        });
    }
    
    /*
    $('#tagsForm').on("submit", false, function(){ 
        var thisForm = $(this), thisDiv = thisForm.next(), comma = $("input[type=text]", thisForm), tagpost=comma.val().split(','), item_id = $("#itemHiddenId").val();

        $.ajax({
                type: 'POST', 
                dataType: 'json',
                url: '/item/addtags',
                cache: false, 
                data: {tagpost: tagpost, id: item_id},
                success: function(json) { 
                    if(json.success){ 
                        for(var key in json.rtags){
                            var val = json.rtags[key].val, id = json.rtags[key].key;

                            var newTag = $("#toclone", thisForm).clone();
                            newTag.find(".tag").text('#'+val);
                            newTag.attr('id', id+'tag');

                            $('ul', thisDiv).append(newTag);

                            newTag.show();  
                        }
                        $("input[type=text]", thisForm).val('');
                        $(comma).val('');
                    } else if(json.error) {
                        $(".ajaxMessage", thisDiv).removeClass('alert-success').addClass('alert-danger').text(json.error).show();
                        setTimeout(function(){$(".ajaxMessage", thisDiv).fadeOut('slow');},5000);
                    }
                },
                error: function(){
                    $(".ajaxMessage", thisDiv).removeClass('alert-success').addClass('alert-danger').text('Unexpected error').show();
                    setTimeout(function(){$(".ajaxMessage", thisDiv).fadeOut('slow');},5000);
            }});
        return false;
    });

    $('#tagsUl').on('click', '.deleteTag', false, function(){
        $(this).removeClass('deleteTag')
        var thisDiv = $(this).parent().parent().parent(), parent = $(this).parent(), id = parent.attr("id");

        $.ajax({
            type: 'POST', 
            dataType: 'json',
            url: '/item/deletetag',
            cache: false, 
            data: {id: id},
            success: function(json) {
                if(json.success){ 
                    parent.fadeOut('fast', function() {parent.remove()});
                } else if(json.error){
                    $(".ajaxMessage", thisDiv).removeClass('alert-success').addClass('alert-danger').text(json.error).show();
                    setTimeout(function(){$(".ajaxMessage", thisDiv).fadeOut('slow');},5000);
                }
            },
            error: function(){
                $(".ajaxMessage", thisDiv).removeClass('alert-success').addClass('alert-danger').text('Unexpected error').show();
                setTimeout(function(){$(".ajaxMessage", thisDiv).fadeOut('slow');},5000);
        }});

        return false;
    });*/

});
{/literal}
</script>
</body>
</html>