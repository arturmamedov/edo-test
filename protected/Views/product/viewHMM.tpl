{include file='layout/head.tpl'}
<meta property="og:url" content="http://www.aidashoptour.com/shop/view/id/{$page->id}/{$page->url}" /> 
<meta property="og:site_name" content="AidaShopTour" /> 
<meta property="og:locale" content="ru" /> 
<meta property="og:type" content="place" /> 
<meta property="og:title" content="{$title}" />
<meta property="og:description" content="{$description}" /> 
<meta property="og:image" content="{$page->poster}" />
<meta property="og:updated_time" content="{$page->ts_updated|date_format:"%Y-%m-%dT%H:%M:%S0"}" />
</head>

<body id="single">
        {include file='layout/navbar.tpl'}

<div class="container">		
    <article class="row">
        <div class="page-header">
            <h1 class="text-center">{$page->content_title}</h1>
        </div>

        <div class="clearfix"></div>   
        <div class="divider"></div>  

        <div class="col-xs-12 bg-tour">
            {$page->content}
        </div>
        
        <div class="clearfix"></div>     
        <div class="divider"></div>
        
        <div class="bg-tour row">
            <div class="container table table-noborder table-hover table-striped">
                <div class="row table-auto-head">
                    <div class="col-sm-6">Машина <small>(и характеристики)</small></div>
                    <div class="text-primary col-sm-2"><strong>1 <small>день</small></strong></div>
                    <div class="text-success col-sm-2"><strong>2 <small>дня</small></strong></div>
                    <div class="text-warning col-sm-2"><strong>3 <small>дня</small></strong></div>
                </div>
                <div class="row">
                    {foreach from=$autos item=auto}
                        <div class="cotainer table-auto">
                            <div class="col-sm-6 table-auto-cell">
                                <figure class="thumbnail col-sm-6">
                                    <img src="{$auto.image_url}" title="{$auto.image_title}" alt="{$auto.image_name}" class="img-responsive"/>
                                </figure>
                                <article class="col-sm-6">
                                    <h3 class="text-center">{$auto.auto_name}</h3>
                                    <hr>
                                    <p class="lead">{$auto.auto_desc|nl2br}</p>
                                </article>
                            </div>
                            <div class="col-sm-2 table-auto-cell text-center margin-top-30">
                                <h6 class="text-primary lead visible-xs"><strong>1 <small>день</small></strong></h6>
                                <h5 class="lead text-primary badge badge-auto-price">{$auto.day_1}€</h5></div>
                            <div class="col-sm-2 table-auto-cell text-center margin-top-30">
                                <h6 class="text-primary lead visible-xs"><strong>2 <small>дня</small></strong></h6>
                                <h5 class="lead text-success badge badge-auto-price">{$auto.day_2}€</h5></div>
                            <div class="col-sm-2 table-auto-cell text-center margin-top-30">
                                <h6 class="text-primary lead visible-xs"><strong>3 <small>дня</small></strong></h6>
                                <h5 class="lead text-warning badge badge-auto-price">{$auto.day_3}€</h5></div>
                            <div class="clearfix"></div>
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
                
        <div class="clearfix"></div>     
        <div class="divider"></div>     


        {include file='layout/contact_info.tpl' hide=true}		
    </article>


</div> <!-- container -->
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl'}

{include file="widget/gallery.tpl"}

</body>
</html>