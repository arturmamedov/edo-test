{include file='layout/head.tpl' close=true}
<body>
    {include file='layout/navbar.tpl'}
    
<div class="container">
    <div class="col-sm-10 center-block jumbotron">
        <div class="page-header">
            <h2 class="text-center margin-top-30">Aggiungi Prodotto</h2>
        </div>
		
        <form class="" method="POST" action="/product/add">
            <div class="form-group row {if $fp->getError(" name ")}has-error{/if}">
                <label class="col-xs-12 col-sm-4 control-label col-sm" for="name">Nome Prodotto</label>
                <div class="col-xs-12 col-sm-8">
                    <input id="name" class="form-control" name="name" value="{$fp->name|escape}" type="text" placeholder="Inserisci il nome del prodotto" />
                    <span class="help-block">{include file="lib/error.tpl" error=$fp->getError("name")}</span>
                </div>
            </div>

            <div class="form-group row {if $fp->getError(" brand ")}has-error{/if}">
                <label class="col-xs-12 col-sm-4 control-label " for="brand">Brand</label>
                <div class="col-xs-12 col-sm-8 ">
                    <input id="brand" class="form-control" name="brand" value="{$fp->brand|escape}" type="text" placeholder="Inserisci nome del brand" />
                    <span class="help-block">{include file="lib/error.tpl" error=$fp->getError("brand")}</span>
                </div>
            </div>

            <div class="form-group row {if $fp->getError(" label ")}has-error{/if}">
                <label class="col-xs-12 col-sm-4 control-label " for="label">Ingradienti</label>
                <div class="col-xs-12 col-sm-8 ">
                    <textarea id="label" class="form-control" placeholder="Inserisci gli ingradienti *" name="label">{$fp->label|escape}</textarea>
                    <span class="help-block">
                        {include file="lib/error.tpl" error=$fp->getError("label")}</span>
                    <small>
                    * Separati da "punto", "virgola" o "punto e virgola"
                    <br>
                    "due punti" o "parentesi tonda" per unire "padre" con quella "figlia"
                    </small>
                </div>
            </div>
                    
            <div class="form-group text-center">
                <input class="btn btn-lg btn-success" value="Inserisci!" type="submit">
            </div>
        </form>
    </div>
</div><!--/container-->

</div><!--/wrap-->

{include file='layout/footer.tpl' hide=true}

{include file='layout/foot.tpl'}

<!-- JavaScript jQuery-->
{literal}
<script type='text/javascript'>
$(document).ready(function() {
});
{/literal}
</script>
</body>
</html>