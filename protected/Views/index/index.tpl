{include file='layout/head.tpl' close=true}
<body>
    {include file='layout/navbar.tpl'}
    
<div class="container">
    <div class="col-sm-10 col-sm-offset-1 jumbotron" id="section2">
		<h1 class="text-center margin-top-30 logo-font">EdoTest</h1>
		<h2 class="text-center margin-top-30">Test Tecnico - EDO</h2>
		
        <p class="text-black lead">Il test è suddiviso in 3 parti</p>
        
        <ol class="">
            <li>Implementazione di un sistema web client-server.</li>
            <li>Implementazione di una ricerca testuale libera</li>
            <li>Ottimizzazione di una query su un database di esempio</li>
        </ol>
        
        
        <h3 class="text-warning text-center">Tutti i prodotti</h3>
        
        <ul class="list-group">
        {foreach from=$products item=p}
            <li class="list-group-item">
                <small class="pull-left badge bg-success">#{$p->id}</small>
                <h3 class="text-center">{$p->name} - <span class="text-danger">{$p->brand}</span></h3>
                <small>
                    Ingradienti:
                    <strong>{$p->label}</strong>
                </small>
                
                <div class="pull-right">
                    <a href="/product/edit/{$p->id}" class="">Modifica</a>
                    &nbsp;
                    <a href="/product/delete/{$p->id}" class="text-danger">Elimina</a> 
                </div>
                <div class="clearfix"></div>
            </li>
        {foreachelse}
            <h3 class="text-center">Nessun prodotto nel database ... <a href="/product/add">Inserisci il primo</a></h3>
        {/foreach}
        </ul>
  	</div><!--/col-->
</div>
    
<div class="divider"></div>
 
<section class="bg-3">
    <div class="col-sm-6 col-sm-offset-3 text-center">
        <h2>Ciao</h2>
        
        <p class="text-black lead">:)</p>
    </div>
</section>

</div><!--/wrap-->

{include file='layout/footer.tpl' hide=true}

{include file='layout/foot.tpl'}

<!-- JavaScript jQuery-->
{literal}
<script type='text/javascript'>
$(document).ready(function() {
});
{/literal}
</script>
</body>
</html>