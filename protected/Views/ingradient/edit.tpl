{include file='layout/head.tpl' close=true}
<body>
    {include file='layout/navbar.tpl'}
    
<div class="container">
    <div class="col-sm-10 center-block jumbotron">
        <div class="page-header">
            <h2 class="text-center margin-top-30">Modifica Ingradiente</h2>
        </div>
		
        <form class="" method="POST" action="/ingradient/edit/{$fp->_item->id}">
            {* <input type="hidden" name="id" value="{$fp->_item->id}" /> *}
            <div class="form-group row {if $fp->getError(" name ")}has-error{/if}">
                <label class="col-xs-12 col-sm-4 control-label col-sm" for="name">Nome Ingradiente</label>
                <div class="col-xs-12 col-sm-8">
                    <input id="name" class="form-control" name="name" value="{$fp->name|escape}" type="text" placeholder="Inserisci il nome del ingradiente" />
                    <span class="help-block">{include file="lib/error.tpl" error=$fp->getError("name")}</span>
                </div>
            </div>
                    
            <div class="form-group text-center">
                <input class="btn btn-lg btn-success" value="Modifica" type="submit">
                <a href="/" class="btn btn-default">Annulla</a>
            </div>
        </form>
    </div>
</div><!--/container-->

</div><!--/wrap-->

{include file='layout/footer.tpl' hide=true}

{include file='layout/foot.tpl'}

<!-- JavaScript jQuery-->
{literal}
<script type='text/javascript'>
$(document).ready(function() {
});
{/literal}
</script>
</body>
</html>