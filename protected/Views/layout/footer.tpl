<div id="footer">
    <h3 class="text-center text-white">By Artur Mamedov (aka) [#withArtur] OBiV.it</h3>
    <h4 class="text-center text-white">2015</h4>
</div>

<ul class="nav pull-right scroll-top">
  <li><a href="javascript:;" title="Scroll to top"><i class="glyphicon glyphicon-chevron-up"></i></a></li>
</ul>