<nav class="navbar navbar-fixed-top navbar-default" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand brand-font" href="/">EdoTest</a>
		</div>
		
		<div class="navbar-collapse1 navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="/product/add" class=""><strong>Aggiungi Prodotto</strong></a></li>
				<li><a href="/product/manage" class=""><strong>Gestisci Prodotti</strong></a></li>
                
                <li class="dropdown">
					<a href="#" class="dropdown-toggle " data-toggle="dropdown">
						<strong>Menu Ingradienti</strong>
						<b class="caret"></b>
					</a>
						<ul class="dropdown-menu">
                            <li><a href="/ingradient/add" class=""><i class="glyphicon glyphicon-plus-sign text-success"></i> &nbsp; <strong>Aggiungi Ingradiente</strong></a></li>
						</ul>
				</li>
			</ul>
			
			
			{*}<form class="navbar-form navbar-right" role="search">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Cerca...">
				</div>
				<button type="submit" class="btn btn-default">Cerca</button>
			</form>{/*}
		</div>
	</div>
</nav>
{include file="lib/pop_message.tpl"}