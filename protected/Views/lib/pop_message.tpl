{if isset($message)}
<div class="alert alert-{$message_type} alert-dismissable withAlert">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <span class="message">{$message}</span>
</div>
{/if}