<div class="masthead">
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <img src="/public/files/slider/images-banner-top-300/bari.jpg" alt="...">
          <div class="carousel-caption">
            Caption 1
          </div>
        </div>

        <div class="item">
          <img src="/public/files/slider/images-banner-top-300/milan.jpg" alt="...">
          <div class="carousel-caption">
            Caption 2
          </div>
        </div>

        <div class="item">
          <img src="/public/files/slider/images-banner-top-300/italia-redvalentine.jpg" alt="...">
          <div class="carousel-caption">
            Caption 3
          </div>
        </div>

        <div class="item">
          <img src="/public/files/slider/images-banner-top-300/italia-venezia.jpg" alt="...">
          <div class="carousel-caption">
            Caption 4
          </div>
        </div>

        <div class="item">
          <img src="/public/files/slider/images-banner-top-300/italia-roma.jpg" alt="...">
          <div class="carousel-caption">
            Caption 5
          </div>
        </div>

      </div>

      <!-- Controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>  
    </div>
    <!-- /.carousel -->
</div>