<link href="/public/css/flexslider.css" rel="stylesheet" type="text/css"/>

<script src="/public/js/jquery.flexslider-min.js" type="text/javascript"></script>

<!-- gMap PLUGIN -->
<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false"></script>
<script src="/public/js/jquery.gmap.min.js"></script>

<script>
{literal}
$(document).ready(function(){    
    // The slider being synced must be initialized first
    $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 104, // + 2px for border
        //itemMargin: 5, // from css
        asNavFor: '#slider'
      });
    $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel",
        start: function() {
            // after images are loaded , position vertical center!
            $('.slides li', "#slider").each( function() {
                var height = $(this).height();

                var imageHeight = $(this).find('img').height();

                var offset = (height - imageHeight) / 2;

                $(this).find('img').css('margin-top', offset + 'px');

            });
        }
    });

            
	var $map = $('#map');
	var latitude = $("#addrname").attr('data-latitude'), 
		longitude = $("#addrname").attr('data-longitude'),
		name = $("#addrname").text();
	
	var myOptions = {
		zoom: 9, 
		center: new google.maps.LatLng(latitude, longitude),
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false
	};
	
	var map = new google.maps.Map($("#map")[0], myOptions);

	infowindow = new google.maps.InfoWindow({
		content: '<div class="marker-info-win" style="line-height:1.35;overflow:hidden;white-space:nowrap;">' + 
			'<div class="marker-inner-win"><div class="info-content">' + 
			'<h4 class="marker-heading no-margin text-center">' + name + '</h4>' +
			'</div></div>'
	});

	// Add new marker
	var marker = new google.maps.Marker({
		position: myOptions.center,
		map: map,
		title: name,
		//icon: '/public/img/site/gmap/007_pointer.png',
		animation: google.maps.Animation.BOUNCE
	});
	
	infowindow.open(map,marker);
	
	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
	});
});
{/literal}
</script>