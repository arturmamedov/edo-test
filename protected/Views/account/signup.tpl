{include file='layout/head.tpl'}
{*}<script type="text/javascript">{literal}var RecaptchaOptions = {theme:'clean', tabindex:6};{/literal}</script>{/*}
</head>

<body>
{include file='layout/menu.tpl'}

<div class="pageclear-120"></div>

<div class="box box-s4 box-bg1 box-center">
    <h1 class="text-center">Registrazione</h1>
    <hr>
    
    <form  method="POST" action="/account/signup" autocomplete="on">
        <div class="box box-s2 box-center text-center">
            <p class="alert alert-error blackText"{if !$fp->hasError()} style="display: none"{/if} >ERROR: Controlla i campi evidenziati</p>	
            {if isset($message)}<div class="alert alert-{$msg_type}">{$message}</div>{/if}
            <div class="clearfix"></div>



            <label class="lfloat" for="firstName">Nome: </label>
            <input tabindex="1" class="rfloat" id="firstName" autofocus="autofocus" name="first_name" required="required" value="{$fp->first_name|escape}" type="text" placeholder="FIRSTNAME" />
            {include file='lib/error.tpl' error=$fp->getError('first_name')}</li>

            <div class="clearfix"></div>

            <label class="lfloat" for="lastName">Cognome: </label>
            <input tabindex="2" class="rfloat" id="lastName" name="last_name" required="required" value="{$fp->last_name|escape}" type="text" placeholder="LASTNAME" />
            {include file='lib/error.tpl' error=$fp->getError('last_name')}

            <div class="clearfix"></div>

            <label class="lfloat" for="mail">Mail: </label>
            <input class="rfloat" tabindex="3" id="mail" name="mail" required="required" type="email" value="{$fp->mail|escape}" placeholder="MAIL" />
            {include file='lib/error.tpl' error=$fp->getError('mail')}

            <div class="clearfix"></div>

            <label class="lfloat" for="password">Password: </label>
            <input class="rfloat" tabindex="4" id="password" name="password" required="required" type="password" value="{$fp->password|escape}" placeholder="PASSWORD">
            {include file='lib/error.tpl' error=$fp->getError('password')}

            <div class="clearfix"></div>

            <label class="lfloat" for="passwordConfirm">Conferma Password: </label>
            <input class="rfloat" tabindex="5" id="passwordConfirm" name="password_confirm" required="required" type="password" value="{$fp->password_confirm|escape}" placeholder="REPEAT_PASSWORD" />
            {include file='lib/error.tpl' error=$fp->getError('password_confirm')}



            {*}<div class="clearfix"></div>

            <ul class="box box-p-largest">
                <li class="box-px-small5">
                    <div id="formCaptcha" class="reCaptcha">{$recaptcha}</div>
                    {include file='lib/error.tpl' error=$fp->getError('captcha')}
                </li>
            </ul>{/*}

            <div class="pageclear-40"></div>

            <p>Cliccando Registrazione si accettano <a href="/index/privacy" target="_blank">i nostri termini di servizio</a></p>

            <input class="btn btn-large btn-success"  tabindex="8" type="submit" name="submit" value="Registrazione"/>


            <a class="rfloat btn btn-mini" href="/account/login">o Logi in</a>
        </div>
        <div class="clearfix"></div>
    </form>
</div>

{include file='layout/footer.tpl'}
</body>
</html>