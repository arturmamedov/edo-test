{include file='layout/head.tpl'}
</head>

<body id="index">
	{include file='layout/navbar.tpl'}
<div class="container">

<!--login modal-->
<div class="show">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<h1 class="text-center">Логин</h1>
		</div>
		<div class="modal-body">
			<form method="POST" action="/account/signin" autocomplete="on" class="form col-md-12 center-block">
				<div class="form-group">
					<input id="mail" name="mail" required="required" type="email" value="{$mail|default:''}" class="form-control input-lg" placeholder="eMail@exampl.com">
				</div>
				<div class="form-group">
					<input id="password" name="password" required="required" type="password" value="{$password|default:''}" class="form-control input-lg" placeholder="Password">
				</div>
				<div class="form-group">
					<input class="btn btn-lg btn-block btn-success" type="submit" name="submit" value="Войти!"/>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<div class="col-md-12">
				АидаШопТоур разрешает вход только администраторам
			</div>	
		</div>
	</div>
</div>
</div>

	{*}
	<div class="container">
		<h1 class="text-center">Login</h1>
		<hr>
		
		<form  method="POST" action="/account/signin" autocomplete="on">
			<div class="box box-s2 box-center text-center">
				{if isset($message)}<div class="alert alert-{$msg_type}">{$message}</div>{/if}
				<div class="clearfix"></div>

				<label class="lfloat" for="mail">Mail: </label>
				<input class="rfloat" tabindex="3" id="mail" name="mail" required="required" type="email" value="{$mail|default:''}" placeholder="MAIL" />

				<div class="clearfix"></div>

				<label class="lfloat" for="password">Password: </label>
				<input class="rfloat" tabindex="4" id="password" name="password" required="required" type="password" value="{$password|default:''}" placeholder="PASSWORD">

				<div class="clearfix"></div>
				
				<input class="btn btn-large btn-success"  tabindex="8" type="submit" name="submit" value="Login"/>

				<a class="rfloat btn btn-mini" href="/account/login">o Sign in</a>
			</div>
			<div class="clearfix"></div>
		</form>
	</div>
	{/*}
	{include file='layout/footer.tpl'}
</div> <!-- container -->

{include file='layout/foot.tpl'}
</body>
</html>