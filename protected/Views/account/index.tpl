{include file='layout/head.tpl'}
</head>

<body id="single">
	{include file='layout/navbar.tpl'}

<div class="container">
    {if isset($message)}
        <div class="alert alert-{$message_type}">{$message}</div>
    {/if}

    <div class="col-sm-8 col-md-6 center-block bg-tour">
        
    <div class="page-header text-center">
        <h1>Ciao {$identity->first_name|default:''}!</h1>
        <p>Da qui gestirai il tuo account :)</p>
    </div>
    
        <p>Можно например: 
            <a class="btn btn-success" href="/tour/manage">Редактировать туры <span class="glyphicon glyphicon-pencil"></span></a>
            <a class="btn btn-danger" href="/shop/manage">Редактировать шопы <span class="glyphicon glyphicon-pencil"></span></a>
        </p>
        
    <hr>
    
    <table class="table table-striped table-responsive">
        <tbody>
            <tr><td>Nome: </td> <td>{$identity->first_name}</td></tr>
            <tr><td>Cognome: </td> <td>{$identity->last_name}</td></tr>
            <tr><td>Mail: </td> <td>{$identity->mail}</td></tr>
            <tr><td>Registrato il: </td> <td>{$user->ts_created}</td></tr>
            <tr><td>Ultimo accesso: </td> <td>{$user->ts_lastlog}</td></tr>
            <tr><td>Attivo?: </td> <td>{$identity->active}</td></tr>
        </tbody>
    </table>
        
    </div>
</div>



{include file='layout/footer.tpl'}
</body>
</html>