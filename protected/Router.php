<?php
// la sessione prima di tutto!
session_start();
/** Artur Mamedov - arturmamedov1993@gmail.com
 * router.php 
 * This controller routes
 * all incoming requests to the appropriate controller
 */
 

/**
 * Auto includes files containing classes that are called
 * 
 * @param type $className
 */
function my_autoloader($className){
    // Parse out filename where class should be located
    // This supports names like 'Example_Model' as well as 'Example_Two_Model'
    if(preg_match("/_/", $className) == 1){
        list($suffix, $filename) = preg_split('/_/', $className); // for revert but i dislike strrev($className)
        
        //select the folder where class should be located based on suffix and compose file name
        switch($suffix) {  
            // actualy not in use, require some modification and thinking about
            case'Model':        
                $folder = DS.'protected'.DS.'Models'.DS;
                $file = ROOT.$folder.$filename.'.php';
                break;
            /*case'Class': not in use
                $folder = DS.'protected'.DS;
                $file = ROOT.$folder.DS.$filename.'.php';
                break;*/
            case'FormProcessor':
                $folder = DS.'protected'.DS.'FormProcessor'.DS;
                $file = ROOT.$folder.$filename.'.php';
            // cant put a default: because can be class that was included manualy
        }
        
        // if nothing defined this can be a call to libraries
        if(!isset($folder)){
            $folder = DS.'protected'.DS.'libraries'.DS.$suffix.DS;
            $file = ROOT.$folder.$filename.'.php';
        }
    } else {
        $folder = DS.'protected'.DS;
        $file = ROOT.$folder.$className.'.php';
    }
    
    //fetch file
    if(file_exists($file))
        require_once($file); //get file
    //else 
        //die("Il File '{$file}' contenente la classe '{$className}' non si trova nella cartella '{$folder}'"); //file does not exist!
}
spl_autoload_register('my_autoloader');


// default request controller and action
$req_controller = 'Index';
$req_action = 'index';

/**
 * Sanitize !!!! @todo To sanitize in right way all param
 */

$request = $_SERVER['QUERY_STRING']; //fetch the passed request

$parsed = explode('/' , $request); //parse the page request and other GET variables
$tmp_parsed = null;

$getVars = array(); //the rest of the array are get statements, parse them out.

// @todo: my Routers
$routes[] = array('case' => '^shopping/%s/?$', 'controller' => 'shop', 'action' => 'view', 0 => 1);
//$routes[] = array('case' => '^shopping/%s/%d/%s/%s$', 'controller' => 'city', 'action' => 'index', 0 => 'italy', 1 => 'number', 2 => 'word', 3 => 'act'); // @fordebug

// loop that control if there are a coincidence in routes array
foreach($routes as $r){
    $case = '#'. preg_replace('/\%d/', '(\d+)', preg_replace('/\%s/', '(.*)', $r['case'])) .'#';
    if(preg_match($case, $request) === 1){
        $occur = $r;
    }
}

if(isset($occur)){
	$req_controller = ucfirst($occur['controller']);
	$req_action = $occur['action'];

	// loop for pass to getVars and after to $this->_get[] the key and value
	$c_val_occur = count($occur) - 3; // occur count of value // if max index
	$c_parsed = count($parsed); // count of parsed
	if(empty($parsed[$c_parsed-1])){
		$c_parsed--;
    }
        
	$bindex_parsed = abs(($c_val_occur - $c_parsed)); // begin index of parsed parmas to insert in value
	
	// hmm wont to make that if /parm/prm/prm/parma/param are more then true situations and routes array, app dont look
	//if(!($c_val_occur != $bindex_parsed))
		//$bindex_parsed--;
	for($i=0,$c=$bindex_parsed;$i<$c_val_occur;$i++,$c++){
        $getVars[$occur[$i]] = urldecode($parsed[$c]);
	}
	
	/*
	// exit(print_r($getVars)); // @fordebug
		Array ( [italy] => bari [number] => 333 [word] => soul [act] => edit )
	*/
	
} elseif(count($parsed) >= 2 && strlen($parsed[1]) != 0){
    // parsed[0] have the first part of url that contain controller name
    $req_controller = ucfirst($parsed[0]);
	array_shift($parsed);
    // parse for action and GET params
    $tmp_parsed = explode('&' , $parsed[0]);
	$req_action = $tmp_parsed[0];
	array_shift($parsed);
	array_shift($tmp_parsed);
	//exit(print_r($tmp_parsed));
} elseif(strlen($parsed[0]) != 0){
    $tmp_parsed = explode('&' , $parsed[0]); // parse for action and GET params
    $req_controller = ucfirst($tmp_parsed[0]);
	array_shift($tmp_parsed);
	array_shift($parsed);
    $req_action = 'index';
}

if(!isset($occur) && count($tmp_parsed) > 0){
	// if uri contain param=value
	foreach($tmp_parsed as $argument){
		//split GET vars along '=' symbol to separate variable, values
		list($variable , $value) = explode('=', $argument);
        $getVars[$variable] = urldecode($value);
	}
} elseif(count($parsed) > 1){
	// if uri contain /param/value/ 
	for($i=0;$i<count($parsed)/2;$i++){
		// add param / value  splitted by slash /
		$getVars[$parsed[$i]] = urldecode($parsed[$i+1]);
	}
} elseif(count($parsed) == 1){
	// for lazy people :) default user param after controller/action/"user_param"
	if(!empty($parsed[0])){
		$getVars['user_param'] = urldecode($parsed[0]);
	}
}


$target = ROOT.DS.'protected'.DS.'Controllers'.DS.$req_controller.'.php'; // path to the file

if(file_exists($target)){
    require_once($target); //get target
    // prepare name class 
    $class = $req_controller.'_Controller';
    
    //instantiate the appropriate class
    if(class_exists($class)){
        $controller = new $class;
        
        $action = $req_action.'Action';
        // @old_view //$controller->view->setView($req_controller, $req_action);
        $controller->view->path = strtolower($req_controller);
        $controller->view->file = strtolower($req_action);
    } else {
        die('Errori nel sistema! Ci scusiamo per il disaggio. <br> Contattare l\'amministratore'); //did we name our class correctly?
    }
} else {
    die('La pagina non esiste!'); //can't find the file in 'controllers'! 
}
// assign our get param to controller array for simply accessing
$controller->_get = $getVars;

//once we have the controller instantiated, execute the default function
$controller->_init(); // INIT
//pass any GET varaibles to the main method
$controller->$action();
// @old_view //$controller->view->render();
$controller->view->display();