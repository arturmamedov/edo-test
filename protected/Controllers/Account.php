<?php
class Account_Controller extends ApplicationController 
{	
    
    public function indexAction()
    {
        // se utente non loggato redirigiamo sulla pagina di signin
        if(!$this->authenticated)
            header('Location: /account/signin');
        
        if(isset($_SESSION['message'])){
            $this->view->message = $_SESSION['message'];
            $this->view->message_type = $_SESSION['message_type'];
            $_SESSION['message'] = NULL;
            $_SESSION['type'] = NULL;
        }
        
        
        $user = new Model_User($this->db);
        $user->load($this->identity->user_id);
        $this->view->user = $user;
        // or, select manually from db
        //$this->db->query("SELECT * FROM users WHERE user_id = {$this->identity->user_id}");
        //$this->view->user = $this->db->fetch('FETCH_OBJ');
        
        // required in all action
        $this->view->title = 'Il tuo account';
        $this->view->description = 'Il tuo account';
    }
    
    public function signinAction(){
        // se utente loggato redirigiamo sulla index di account
        if($this->authenticated)
            header('Location: /account/index');
        
        if($_POST){
            $mail = $_POST['mail'];
            $psw = $_POST['password'];

            $user = new Model_User($this->db);

            if($user->authenticate($mail, $psw))
                header('Location: /account');
            else {
                $_SESSION['message'] = 'Le tue credenziali non corrispondono a nessun utente';
                $_SESSION['message_type'] = 'error';
            }
        }
        
        if(isset($_SESSION['message'])){
            $this->view->message = $_SESSION['message'];
            $this->view->message_type = $_SESSION['message_type'];
            $_SESSION['message'] = NULL;
            $_SESSION['type'] = NULL;
        }
        
        
        // required in all action
        $this->view->title = 'Login';
        $this->view->description = 'Login';
    }
    
    /*
    public function signupAction(){
        // se utente loggato redirigiamo sulla index di account (non lo faremo mica registrare un altra volta)
        if($this->authenticated)
            header('Location: /account/index');
        
        $fp = new FormProcessor_UserSignup($this->db);
     
        if(isset($_POST['submit'])){
            if($fp->process($_POST)){
                $fp->user->createIdentity();
                $_SESSION['message'] = 'Ti sei registrato correttamente '.$this->identity->first_name.', grazie per l\'interesse';
                $_SESSION['message_type'] = 'success';
                header('Location: /account/index');
            }
        }
        
        $this->view->fp = $fp;
        // required in all action
        $this->view->title = 'MVC Framework | withArtur';
        $this->view->keywords = 'Artur, Mamemedov, withArtur, MVC, Framework';
        $this->view->description = 'Artur Mamedov aka withArtur show how do flexible little MVC Framework ';
    }
    */
    
    public function signoutAction(){
        // eliminiamo la sessione e ridirigiamo sulla index per il log out
        session_destroy();
        header('Location: /account/index');
    }
}
?>