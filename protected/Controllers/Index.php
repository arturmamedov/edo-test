<?php
class Index_Controller extends ApplicationController 
{	
    public function indexAction()
    {
        $this->view->products = Model_Product::Get($this->db);
        
        // required in all action
        $this->view->title = 'Home';
        $this->view->description = 'Home page';
    }
}