<?php
class Ingradient_Controller extends ApplicationController 
{
//    public function manageAction()
//    {
//        $this->view->products = Model_Ingradient::Get($this->db);
//        
//        // required in all action
//        $this->view->title = 'Gestisci ingradienti';
//        $this->view->description = 'Gestisci Ingradienti';
//    }
//    
//    public function searchAction()
//    {
//        $term = $this->sanitize($this->_get['term']);
//        
//        $options = array('search' => $term);
//        
//        $products = Model_Ingradient::Get($this->db, $options);
//        
//        echo json_encode(array('success' => true, 'products' => $products));
//        exit;
//    }
    
    /**
     * Restituisce i dati JSON di tutti gli ingradienti
     * che riceve come stringa tramite POST
     * - controlla anche se sono gia collegati al prodotto
     * - e se ci sono gia nella tabella ingradients dei ingradienti
     */
    public function getAction()
    {
        $ingradients = $this->sanitize($_POST['ingradients']);
        $product_id = $this->sanitize($_POST['product_id']);
        
        $model = new Model_Ingradient($this->db);
        
        // ingradients array from string
        $ings = $model->getFromString($ingradients);
        
        // check if ing isset in db and if its related yet to product
        $checked_ings = array();
        foreach($ings as $k => $ing){
            $checked_ings[$k]['name'] = $ing;
            
            $checked_ings[$k]['saved'] = $model->saved($ing);
            if($checked_ings[$k]['saved']){
                $checked_ings[$k]['inproduct'] = $model->inProduct($ing, $product_id);
            } else {
                $checked_ings[$k]['inproduct'] = false;
            }
        }
        
        echo json_encode(array('success' => true, 'ings' => $checked_ings));
        exit;
    }
    
	public function addjsonAction()
    {        
        $fp = new FormProcessor_AddIngradient($this->db);
        
        if($_POST){
            if($fp->process($_POST)){
                echo json_encode(array('success' => true));
                exit;
            }
        }
        
        echo json_encode(array('success' => false));
        exit;
    }
	
    
    public function removejsonAction()
    {        
        $product_id = (int)$this->_get['product_id'];
        $ingradient_id = (int)$this->_get['ingradient_id'];
        $_item = new Model_Ingradient($this->db, $ingradient_id, true);
        
        if($_item->_delete($product_id, $ingradient_id)){
            echo json_encode(array('success' => true));
            exit;
        } else {
            echo json_encode(array('success' => false));
            exit;
        }
        
        echo json_encode(array('success' => false));
        exit;
    }
    
    public function createrelationAction()
    {        
        $product_id = (int) $_POST['product_id'];
        $ingradient_id = (int) $_POST['ingradient_id'];
        $ingradient_name = $this->sanitize($_POST['ingradient_name']);
        
        $_item = new Model_IngradientsProducts($this->db);
        
        if($_item->createRelation($product_id, $ingradient_id, $ingradient_name)){
            echo json_encode(array('success' => true));
            exit;
        } else {
            echo json_encode(array('success' => false));
            exit;
        }
        
        echo json_encode(array('success' => false));
        exit;
    }
    
    
	public function addAction()
    {        
        $fp = new FormProcessor_AddIngradient($this->db);
        
        if($_POST){
            if($fp->process($_POST)){
				$_SESSION['message_type'] = 'success';
                $_SESSION['message'] = $fp->success_message;
                
                header("Location: /index");
                exit;
            }
        }
        
        $this->view->fp = $fp;
        
        // required in all action
        $this->view->title = 'Aggiungi Prodotto';
        $this->view->description = 'Aggiungi Prodotto';
    }
	
    public function editAction()
    {        
        $item_id = (int) $this->_get['user_param'];
        $fp = new FormProcessor_EditIngradient($this->db, $item_id, true);
        
        if($_POST){
            if($fp->process($_POST)){
				$_SESSION['message_type'] = 'success';
                $_SESSION['message'] = $fp->success_message;
                
                header("Location: /index");
                exit;
            }
        }
        
        $this->view->fp = $fp;
        
        // required in all action
        $this->view->title = 'Modifica Prodotto';
        $this->view->description = 'Modifica Prodotto';
    }
    
    public function deleteAction()
    {        
        $item_id = (int) $this->_get['user_param'];
        $_item = new Model_Ingradient($this->db, $item_id, true);
        
        if($_item->delete()){
            $_SESSION['message_type'] = 'warning';
                $_SESSION['message'] = 'Prodotto eliminato con successo.';
            
            header("Location: /index");
            return;
        } else {
            $_SESSION['message_type'] = 'danger';
            $_SESSION['message'] = 'Prodotto non eliminato, riprovare.';
                
            header("Location: /index");
            return;
        }
        
        // required in all action
        $this->view->title = 'Elimina Prodotto';
        $this->view->description = 'Elimina Prodotto';
    }
}