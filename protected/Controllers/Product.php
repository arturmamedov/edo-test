<?php
class Product_Controller extends ApplicationController 
{
    public function manageAction()
    {
        $this->view->products = Model_Product::Get($this->db);
        
        // required in all action
        $this->view->title = 'Gestisci prodotti';
        $this->view->description = 'Gestisci Prodotti';
    }
    
    public function searchAction()
    {
        $term = $this->sanitize($this->_get['term']);
        
        $options = array('search' => $term);
        
        $products = Model_Product::Get($this->db, $options);
        
        echo json_encode(array('success' => true, 'products' => $products));
        exit;
    }
    
    /**
     * Restituisce i dati JSON di un solo prodotto
     * riceve tramite get il suo id
     */
    public function getAction()
    {
        $id = (int) $this->_get['user_param'];
        
        $options = array('id' => $id);
        
        $product = Model_Product::Get($this->db, $options);
        
        echo json_encode(array('success' => true, 'product' => $product));
        exit;
    }
    
	public function addAction(){        
        $fp = new FormProcessor_AddProduct($this->db);
        
        if($_POST){
            if($fp->process($_POST)){
				$_SESSION['message_type'] = 'success';
                $_SESSION['message'] = $fp->success_message;
                
                header("Location: /index");
                exit;
            }
        }
        
        $this->view->fp = $fp;
        
        // required in all action
        $this->view->title = 'Aggiungi Prodotto';
        $this->view->description = 'Aggiungi Prodotto';
    }
	
    public function editAction()
    {        
        $item_id = (int) $this->_get['user_param'];
        $fp = new FormProcessor_EditProduct($this->db, $item_id, true);
        
        if($_POST){
            if($fp->process($_POST)){
				$_SESSION['message_type'] = 'success';
                $_SESSION['message'] = $fp->success_message;
                
                header("Location: /index");
                exit;
            }
        }
        
        $this->view->fp = $fp;
        
        // required in all action
        $this->view->title = 'Modifica Prodotto';
        $this->view->description = 'Modifica Prodotto';
    }
    
    public function deleteAction()
    {        
        $item_id = (int) $this->_get['user_param'];
        
        $_item = new Model_Product($this->db, $item_id, true);
        
        if($_item->delete()){
            $_SESSION['message_type'] = 'warning';
                $_SESSION['message'] = 'Prodotto eliminato con successo.';
            
            header("Location: /index");
            return;
        } else {
            $_SESSION['message_type'] = 'danger';
            $_SESSION['message'] = 'Prodotto non eliminato, riprovare.';
                
            header("Location: /index");
            return;
        }
        
        // required in all action
        $this->view->title = 'Elimina Prodotto';
        $this->view->description = 'Elimina Prodotto';
    }
    
    
    public function viewAction(){
        exit('Not Ready');
    }
}