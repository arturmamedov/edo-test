<?php
class Model_Ingradient extends DataObject {    
    
    /**
     * Model_Page for work with page in database
     *  
     * @param object $db Database Object
     * @param int $idVal if isset a id load from db
     * @param bool $ln load now?
    */
	public function __construct($db, $idVal = null, $ln = false)
    {
        parent::__construct($db, DB_PREFIX.'ingradients', 'id');
        
        // the row that are selected by default load
        $this->add('id');
        $this->add('name');
        
        if($idVal > 0 && $ln == true){
            $this->load($idVal);
        }
        // else set the global var...
    }
    
    protected function preInsert(){
        $this->name = strtolower($this->name);
        return true;
    }
    
    
    public function _delete($product_id, $ingradient_id){
        try{
            $_item = new Model_IngradientsProducts($this->_db);

            if($_item->_delete($ingradient_id) && $this->delete()){
                return true;
            }
        } catch (Exception $e){
            // todo
            exit($e->getMessage());
        }
        
        return false;
    }
    
    /**
     * Return array with all ingradients fetched from sitrng
     * Ogni ingradient e separato da '.' or ',' or ';'
     * Gli ingradient con ':' or '()' devono essere uniti al padre
     * es: "emulsionante: lecitina di soia, lecitina di girasole;" 
     * uniti
     * "emulsionante lecitina di soia, emulsionante lecitina di girasole"
     * es: "emulsionante: ( lecitina di soia, lecitina di girasole)" 
     * uniti
     * "emulsionante lecitina di soia, emulsionante lecitina di girasole"
     * 
     * con due punti: /\w+:[\w\s,]+;/
     * con parentesi: /\w+\s+\([\w\s,]+\);/
     * 
     * @param string $ingradients
     */
    public function getFromString($ingradients)
    {
        // the result
        $ings = array();
        
        // match 1 & 2
        $matches1 = array();
        $matches2 = array();
        
        // regExp 1 & 2
        $points = '/\w+:[\w\s,]*[;,\.]?/';
        $parentesi = '/\w+\s+:?\([\w\s,]+\)[;,\.]?/';
        
        // 1 pregMatch and replace for get the : separated object
        preg_match_all($points, $ingradients, $matches1);
        $ingradients = preg_replace($points, '', $ingradients);
        
        $name1 = ''; // the name of Father in gorup
        $name1childs = array(); // all childs
        foreach($matches1[0] as $m1){
            $pos1 = strpos($m1, ':');
            $name1 = trim(substr($m1, 0, $pos1));
            
            $name1childs = preg_split("/,/", trim(substr($m1, $pos1+1)));
            
            foreach($name1childs as $ch){
                $child = preg_replace('/(;|\.)/', '', $ch);
                $ings[] = $name1.' '.trim($child);
            }
        }
        
        // pregMatch andreplace for get the () separated element
        preg_match_all($parentesi, $ingradients, $matches2);
        $ingradients = preg_replace($parentesi, '', $ingradients);
        
        $name2 = ''; // the name of Father in gorup
        $name2childs = array(); // all childs
        foreach($matches2[0] as $m2){
            $pos2 = strpos($m2, '(');
            $name2 = trim(substr($m2, 0, $pos2));
            
            $name2childs = preg_split("/,/", trim(substr($m2, $pos2+1)));
            
            foreach($name2childs as $ch){
                $child = preg_replace('/(;|\.|\(|\))/', '', $ch);
                $ings[] = $name2.' '.trim($child);
            }
        }
        
        $remainings = preg_split("/(,|\.|\;)/", $ingradients);
        $rems = array();
        foreach($remainings as $r){
            $rem_name = trim(preg_replace('/(;|\.|\(|\))/', '', $r));
            if(strlen($rem_name) > 0){
                $rems[] = $rem_name;
            }
        }
        
        $ings = array_merge($rems, $ings);
        
        return array_map("trim", $ings);
    }
    
    /**
     * Check if a ingradient yet saved in database
     * 
     * @param string $name name of ingradient
     * 
     * @return bool/int id if saved, false another
     */
    public function saved($name){
        
        $query = 'SELECT id FROM '.DB_PREFIX.'ingradients';
        $query .= " WHERE name LIKE '{$name}'";
        
        try{
            $id = $this->_db->fetchOne($query);
            if($id > 0){
                return $id;
            } else {
                return false;
            }
        } catch(Exception $e) {
            // log $e->getMessage()
            return false;
        }
    }
    
    /**
     * Match if ingradient_id are in relation with product_id
     * 
     * @param string $name name of ingradient
     * @param int $product_id id of product to match
     * 
     * @return bool true if saved, false another
     */
    public function inProduct($name, $product_id)
    {    
        $query = 'SELECT id FROM '.DB_PREFIX.'ingradients';
        $query .= " WHERE name LIKE '{$name}'";
        
        $this->_db->query($query);
        
        $finded = $this->_db->fetch(Database_Mysqli::FETCH_NUM);
        if($finded == 1){
            $ing_id = $this->_db->fetchOne($query);
            $ings_prods = new Model_IngradientsProducts($this->_db);
            return $ings_prods->related($product_id, $ing_id);
        } elseif($finded > 1) {
            // todo: write to log, for watch what ingradient are twice in database
            
            $ing_id = $this->_db->fetchOne($query); // maybe error test! MySQL ingradients.name must be UNIQUE for awoid this !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // @todo
            $ings_prods = new Model_IngradientsProducts($this->_db);
            return $ings_prods->related($product_id, $ing_id);
        }
        
        return false;
    }
    
    /**
     * Method for Get all items of this model from database table
     * 
     * @param object/Database_Object $db
     * @param array $options array of options for selection
     * 
     * @return array
     */
	static function Get($db, $options = array())
    {
        // initialize the options
        $defaults = array(
            'id' => array(),
            'search' => '',
            'return' => 'object'
        );

        foreach($defaults as $k => $v) {
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }
        
		$query = 'SELECT * FROM '.DB_PREFIX.'ingradients';
        $where = false;
        
        if(strlen($options['search']) > 0){
            $query .= " WHERE name LIKE '%{$options['search']}%' OR brand LIKE '%{$options['search']}%' ";
            $where = true;
        }
        
        // filter results on specified hospices ids (if any)
        if(count($options['id']) > 1){
            $query .= ($where) ? '' : ' WHERE ';
            $query .= 'id IN ('.  implode($options['id'], ',').')';
            $where = true;
        } elseif(count($options['id']) > 0){
            $query .= ($where) ? '' : ' WHERE ';
            $query .= "id = {$options['id']}";
            $where = true;
        }
		
        $db->query($query);
        
        $data = array();
        switch($options['return']){
            case'array':
                while($result = $db->fetch()){
                    $data[] = $result;
                }
            break;
            case'object':
                while($result = $db->fetch(Database_Mysqli::FETCH_OBJ)){
                    $data[] = $result;
                }
            break;
            default:
                while($result = $db->fetch(Database_Mysqli::FETCH_OBJ)){
                    $data[] = $result;
                }
        }
		
		return $data;
	}
}
