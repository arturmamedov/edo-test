<?php
class Model_Product extends DataObject {    
    
    /**
     * Model_Page for work with page in database
     *  
     * @param object $db Database Object
     * @param int $idVal if isset a id load from db
     * @param bool $ln load now?
    */
	public function __construct($db, $idVal = null, $ln = false)
    {
        parent::__construct($db, DB_PREFIX.'products', 'id');
        
        // the row that are selected by default load
        $this->add('id');
        $this->add('name');
        $this->add('brand');
        $this->add('label');
        
        if($idVal > 0 && $ln == true){
            $this->load($idVal);
        }
        // else set the global var...
    }    
    
    /**
     * Method for Get all items of this model from database table
     * 
     * @param object/Database_Object $db
     * @param array $options array of options for selection
     * 
     * @return array
     */
	static function Get($db, $options = array())
    {
        // initialize the options
        $defaults = array(
            'id' => array(),
            'search' => '',
            'return' => 'object'
        );

        foreach($defaults as $k => $v) {
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }
        
		$query = 'SELECT * FROM '.DB_PREFIX.'products';
        $where = false;
        
        if(strlen($options['search']) > 0){
            $query .= " WHERE name LIKE '%{$options['search']}%' OR brand LIKE '%{$options['search']}%' ";
            $where = true;
        }
        
        // filter results on specified hospices ids (if any)
        if(count($options['id']) > 1){
            $query .= ($where) ? '' : ' WHERE ';
            $query .= 'id IN ('.  implode($options['id'], ',').')';
            $where = true;
        } elseif(count($options['id']) > 0){
            $query .= ($where) ? '' : ' WHERE ';
            $query .= "id = {$options['id']}";
            $where = true;
        }
		
        $db->query($query);
        
        $data = array();
        switch($options['return']){
            case'array':
                while($result = $db->fetch()){
                    $data[] = $result;
                }
            break;
            case'object':
                while($result = $db->fetch(Database_Mysqli::FETCH_OBJ)){
                    $data[] = $result;
                }
            break;
            default:
                while($result = $db->fetch(Database_Mysqli::FETCH_OBJ)){
                    $data[] = $result;
                }
        }
		
		return $data;
	}
}
