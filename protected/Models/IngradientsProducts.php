<?php
class Model_IngradientsProducts extends DataObject 
{    
    /**
     * Model_Page for work with page in database
     */
	public function __construct($db)
    {
        parent::__construct($db, DB_PREFIX.'ingradients_products', 'product_id');
        
        // the row that are selected by default load
        $this->add('product_id');
        $this->add('ingradient_id');
    }
    
    public function _delete($ingradient_id){
        $this->_db->query('DELETE FROM '.DB_PREFIX."{$this->_table} WHERE ingradient_id = {$ingradient_id}");
        
        return true;
    }
    
    /**
     * True if ingrdient and product are yet related in database
     * 
     * @param int $product_id
     * @param int $ingradient_id
     * 
     * @return bool
     */
    public function related($product_id, $ingradient_id)
    {
        $query = 'SELECT count(*) FROM '.DB_PREFIX."ingradients_products WHERE product_id = {$product_id} AND ingradient_id = {$ingradient_id}";
        
        if($this->_db->fetchOne($query) == 1){
            return true;
        }
        
        return false;
    }
    
    
    public function createRelation($product_id, $ingradient_id, $ingradient_name)
    {
        try{
            $product = new Model_Product($this->_db);
            if(!$product->load($product_id)){
                return false;
            }
            $this->product_id = $product_id;
            
            $ingradient = new Model_Ingradient($this->_db);
            if(!$ingradient->load($ingradient_id)){
                $ingradient->name = $ingradient_name;
                if(!$ingradient->save()){
                    return false;
                }
                $ingradient_id = $ingradient->_id;
            }
            $this->ingradient_id = $ingradient_id;
            
            if($this->save()){
                return true;
            }
        } catch (Exception $e){
            //log
            exit($e->getMessage());
        }
        
        return false;
    }

    /**
     * Load product_id by ingradient_id
     * @param type $ingradient_id the id for match
     * @return boolean/int The product_related id OR false
     * /
    public function loadByIngradient($ingradient_id)
    {
        $this->load($ingradient_id, 'ingradient_id');
        
        if($this->product_id > 0){
            return $this->product_id;
        }

        return false;
    }*/


    /**
     * Method for Get all items of this model from database table
     * 
     * @param object/Database_Object $db
     * @param array $options array of options for selection
     * 
     * @return array
     * / // Get va bene ma deve prendere entrambi - cosa che io di solito faccio nel padre cioe Product
	static function Get($db, $options = array())
    {
        // initialize the options
        $defaults = array(
            'id' => array(),
            'search' => '',
            'return' => 'object'
        );

        foreach($defaults as $k => $v) {
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }
        
		$query = 'SELECT * FROM '.DB_PREFIX.'ingradients';
        $where = false;
        
        if(strlen($options['search']) > 0){
            $query .= " WHERE name LIKE '%{$options['search']}%' OR brand LIKE '%{$options['search']}%' ";
            $where = true;
        }
        
        // filter results on specified hospices ids (if any)
        if(count($options['id']) > 1){
            $query .= ($where) ? '' : ' WHERE ';
            $query .= 'id IN ('.  implode($options['id'], ',').')';
            $where = true;
        } elseif(count($options['id']) > 0){
            $query .= ($where) ? '' : ' WHERE ';
            $query .= "id = {$options['id']}";
            $where = true;
        }
		
        $db->query($query);
        
        $data = array();
        switch($options['return']){
            case'array':
                while($result = $db->fetch()){
                    $data[] = $result;
                }
            break;
            case'object':
                while($result = $db->fetch(Database_Mysqli::FETCH_OBJ)){
                    $data[] = $result;
                }
            break;
            default:
                while($result = $db->fetch(Database_Mysqli::FETCH_OBJ)){
                    $data[] = $result;
                }
        }
		
		return $data;
	}*/
}
