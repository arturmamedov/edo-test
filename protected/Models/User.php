<?php
class Model_User extends DataObject {    
    public function __construct($db, $idVal = null, $ln = false){
        
        parent::__construct($db, 'users', 'user_id');
        
        // the row that are selected by default load
        $this->add('user_id');
        $this->add('first_name');
        $this->add('last_name');
        $this->add('password');
        $this->add('mail');
        $this->add('type');
        $this->add('verification_key');
        $this->add('ts_lastlog');
        $this->add('ts_created');
        $this->add('active');
        
        if($idVal > 0 && $ln == true)
            $this->load($idVal);
        // else set the global var...
    }
    
    /**
     * Creazione di un utente
     * Prima di tutto avremo bisogno di una query
     * poi i parametri, in questo caso devono essere registrati direttamente nel oggetto e non passatti alla funzione
     * 
     * @depends Mysqli_Database usa insert(string[query], array[data])
     * 
     * @return boolean/int id del utente nel caso si é inserito, false se un errore é successo @todo ora c'é un exit da gestire meglio.
     */
    public function create(){
        $vkey = substr(uniqid(), 0, 70);
        
        try{
            $data = array(
                'user_id' => NULL,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'mail' =>  $this->mail,
                'password' => md5($this->password),
                'type' => $this->type,
                'verification_key' => $vkey,
                'ts_lastlog' => 'CURRENT_TIMESTAMP', // todo: this dont work because quoted
                'ts_created' => '2013-10-22 00:00:00',
                'active' => '0'
            );
            
            $user_id = $this->_db->insert($this->_table, $data);
        } catch (Exception $e) {
            exit('Un errore - eccolo ma solo a scopo di test, dopo qui bisogna prevedere cosa fare con l\'utente es redirrigerlo per riprovare etc... ERROR: '. $e->getMessage()); // con get trace vediamo molto di piu
        }
        
        return $user_id;
    }
    
    public function createIdentity(){
        //* creiamo loggetto identity da registrare nella sessione per fornire e avere sempre a portata di mano i dati fondamentali del utente
        $identity = new stdClass();
        $identity->user_id = $this->user_id;
        $identity->first_name = $this->first_name;
        $identity->last_name = $this->last_name;
        $identity->mail = $this->mail;
        $identity->type = $this->type;
        $identity->active = $this->active;
        
        $_SESSION['identity'] = $identity;
        
        return true;
    }
    
    
    /**
     * Creazione di un utente
     * Prima di tutto avremo bisogno di una query
     * poi i parametri, in questo caso devono essere registrati direttamente nel oggetto e non passatti alla funzione
     * 
     * @depends Mysqli_Database usa insert(string[query], array[data])
     * 
     * @return boolean/int id del utente nel caso si é inserito, false se un errore é successo @todo ora c'é un exit da gestire meglio.
     */
    public function authenticate($mail, $psw){
        $password = md5($psw);
        
        // try select and fetch user data
        //$this->_db->prepare("SELECT {$this->_idField} FROM {$this->_table} WHERE mail = '{$mail}' AND password = '{$password}'");
        //$this->_db->prepare("SELECT {$this->_idField} FROM {$this->_table} WHERE mail = '{$mail}' AND password = '{$password}'");
        //$this->_db->query();
        
        // new method for get user data from db
        $query = "SELECT {$this->_idField} FROM {$this->_table} WHERE mail = '{$mail}' AND password = '{$password}'";
        $user_id = $this->_db->fetchOne($query);
        if($user_id <= 0)
            return false;
        
        /* now thi is doed in load directly for any object
        foreach($user as $key => $value){
            $this->$key = $value;
        }
         */
        
        $this->load($user_id);
        
        $this->createIdentity();
            
        return true;
    }
}
?>
