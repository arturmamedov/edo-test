<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));
define('PS', PATH_SEPARATOR);

// USER CONF ***@toedit***
define('LOCAL_HTTP_HOST', 'edotest.loc');
define('SITE_NAME', 'EdoTest');

set_include_path(ROOT.DS.'protected'.PS.get_include_path());

if(stripos(LOCAL_HTTP_HOST, $_SERVER['HTTP_HOST']) !== FALSE){
    error_reporting(-1); // DEV
    
    // USER CONF ***@toedit***
    define('DB_PREFIX', '');
    // DataBase
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PSW', '');
    define('DB_NAME', 'edo-test');
} else {
    error_reporting(0); // WORK
    
    // USER CONF ***@toedit***
    require_once ROOT.DS.'protected'.DS.'config'.DS.'config.php';
}

require_once ROOT.DS.'protected'.DS.'Router.php';